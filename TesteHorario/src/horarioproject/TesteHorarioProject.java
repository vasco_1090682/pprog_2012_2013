package horarioproject;

import horario.*;
//import utilitarios.Data;

public class TesteHorarioProject {

    public static void main(String[] args) throws Exception {
        System.out.println("\n1. Inserção por ficheiro de texto de alunos, professores, disciplinas, salas, turmas, e horários.");
        Horario.LerFicheiroAlunos();
        Horario.LerFicheiroProfessores();
        Horario.LerFicheiroSalas();
        Horario.LerFicheiroTurmas();
        Horario.LerFicheiroDisciplinas();
        Horario.LerFicheiroCelulasHorarios();

        System.out.println("\n2. Edição de uma célula do horário de uma qualquer turma (inserção, alteração ou eliminação).");
        System.out.println("\n2.1 Eliminação");
        Horario.ImprimirHorarioTurma("1DA"); //horário antes da eliminação
        Horario.EliminarCelulaTurma("1DA", 2, 14);
        Horario.ImprimirHorarioTurma("1DA"); //horário posterior à eliminação

        System.out.println("\n2.2 Inserção");
        Horario.InserirCelulaTurma("1DA", "POO", "T", 2, 17, 1, "DMR", "B401");
        Horario.ImprimirHorarioTurma("1DA"); //verificar inserção

        System.out.println("\n2.3 Alteração");
        Horario.AlterarCelulaTurma("1DA", "POO", "T", 2, 17, 1, "DMR", "B401", "1DA", "POO", "P", 2, 17, 1, "DMR", "B210");
        Horario.ImprimirHorarioTurma("1DA"); //verificar alteração

        System.out.println("\n3. Listar os alunos de uma turma por:");
        System.out.println("\n3.1 Ordem crescente do número de aluno;");
        Horario.OrdenarAlunosTurmaCrescenteNumero("1DA");
        Horario.ListarAlunosTurma("1DA");

        System.out.println("\n3.2 Ordem decrescente do número de aluno;");
        Horario.OrdenarAlunosTurmaDecrescenteNumero("1DA");
        Horario.ListarAlunosTurma("1DA");

        System.out.println("\n3.3 Ordem ascendente do nome;");
        Horario.OrdenarAlunosTurmaAscendenteNome("1DA");
        Horario.ListarAlunosTurma("1DA");

        System.out.println("\n3.4 Ordem descendente do nome;");
        Horario.OrdenarAlunosTurmaDescendenteNome("1DA");
        Horario.ListarAlunosTurma("1DA");

        System.out.println("\n4. Apresentar o horário semanal (que é sempre o mesmo ao longo do semestre):");
        System.out.println("\n4.1 Do aluno:");
        Horario.ImprimirHorarioAluno(1120808);

        System.out.println("\n4.2 Do professor:");
        Horario.ImprimirHorarioProfessor("DMR");

        System.out.println("\n4.3 Da turma:");
        Horario.ImprimirHorarioTurma("1DA");

        System.out.println("\n4.4 Da disciplina:");
        Horario.ImprimirHorarioDisciplina("POO");

        System.out.println("\n4.5 Da sala:");
        Horario.ImprimirHorarioSala("B210");

        System.out.println("\n5. Apresentar a carga horária semanal de um aluno, professor, turma, disciplina ou sala.");
        System.out.println("Aluno 1110623: " + Horario.CargaHorariaSemanalAluno(1110623));

        System.out.println("Professor DMR: " + Horario.CargaHorariaSemanalProfessor("DMR"));

        System.out.println("Turma 1DA: " + Horario.CargaHorariaSemanalTurma("1DA"));

        System.out.println("Disciplina POO: " + Horario.CargaHorariaSemanalDisciplina("POO"));

        System.out.println("Sala B401: " + Horario.CargaHorariaSemanalSala("B401"));

        System.out.println("\n6. Encontrar o primeiro período disponível (hora e duração) de uma sala");
        int[] str = Horario.EncontrarPeriodoDisponivel("B401", 2, 10);
        System.out.println("Hora: " + str[0] + ":00");
        System.out.println("Duração: " + str[1] + " horas");
        Horario.ImprimirHorarioSala("B401"); //verificar se a hora e a duração estão correctas

        System.out.println("\n7. Listar as salas cuja taxa de ocupação semanal é superior a um determinado valor definido pelo utilizador (e.g., 90%).");
        Horario.ListarSalasOcupadas(40);

        System.out.println("\n8. Listar as disciplinas lecionadas por um determinado professor, indicando as respectivas turmas.");
        Horario.ListarDisciplinasLecionadasProfessor("FAN");

        System.out.println("\n9. Listar as disciplinas que decorrem numa dada sala.");
        Horario.ListarDisciplinasSala("B301");

        System.out.println("\n10. Listar os professores que dão aulas a uma determinada turma, indicando as respetivas disciplinas e tipos de aula.");
        Horario.ListarProfessoresTurma("1DA");

        System.out.println("\n11. Listar os dados de um determinado aluno, ou professor, ou turma, ou disciplina ou sala");
        System.out.println("\n11.1 Dados Aluno");
        Horario.ListarDadosAluno(1110623);

        System.out.println("\n11.2 Dados Professor");
        Horario.ListarDadosProfessor("FAN");

        System.out.println("\n11.3 Dados Turma");
        Horario.ListarDadosTurma("1DA");

        System.out.println("\n11.4 Dados Disciplina");
        Horario.ListarDadosDisciplina("POO");

        System.out.println("\n11.5 Dados Sala");
        Horario.ListarDadosSala("B401");
    }
}
