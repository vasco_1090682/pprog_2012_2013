package horarioproject;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class HorarioUI extends JFrame {

    private JButton cancelar;

    public HorarioUI(String[][] data, String titulo) {
        super(titulo);

        Container c = getContentPane();

        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p2 = new JPanel();
        cancelar = new JButton("Cancelar");
        p2.add(cancelar);

        JPanel p_horario = new JPanel(new GridLayout(11, 6));
        String[] diasExtenso = {"Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira"};
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 6; j++) {
                if (i == 0 && j == 0) {
                    JPanel cantoSupEsq = new JPanel();
                    cantoSupEsq.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Color.black));
                    cantoSupEsq.setBackground(new Color(114, 176, 252));
                    JLabel csq = new JLabel("");
                    cantoSupEsq.add(csq);
                    p_horario.add(cantoSupEsq);
                } else if (i == 0 && j > 0) {
                    JPanel p_dia = new JPanel();
                    p_dia.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 1, Color.black));
                    p_dia.setBackground(new Color(114, 176, 252));
                    JLabel dext = new JLabel(diasExtenso[j - 1]);
                    p_dia.add(dext);
                    p_horario.add(p_dia);
                } else if (j == 0 && i > 0) {
                    JPanel p_hora = new JPanel();
                    p_hora.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Color.black));
                    p_hora.setBackground(new Color(114, 176, 252));
                    JLabel h = new JLabel("" + (i + 7) + ":00");
                    p_hora.add(h);
                    p_horario.add(p_hora);
                } else if (i > 0 && j > 0) {
                    JPanel p_celula = new JPanel();

                    //borders
                    if (data[i - 1][j - 1] != "  ") {
                        if (i == 10) {
                            p_celula.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, Color.black));
                        } else {
                            p_celula.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 1, Color.black));
                        }

                    } else {
                        if (i == 10) {
                            p_celula.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, Color.black));
                        } else {
                            p_celula.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.black));
                        }
                    }

                    //cores para cada tipo aula
                    if (data[i - 1][j - 1] != "  " && data[i - 1][j - 1] != "   " && data[i - 1][j - 1] != null) {
                        String[] campos = data[i - 1][j - 1].split("-");
                        String tipoAula = campos[1].trim();
                        if (tipoAula.equals("P")) {
                            p_celula.setBackground(new Color(240, 125, 129));
                        } else if (tipoAula.equals("T")) {
                            p_celula.setBackground(new Color(251, 254, 112));
                        }
                    }

                    //cores para as 2ª horas
                    if (data[i - 1][j - 1] == "  ") { //segundas horas das práticas
                        p_celula.setBackground(new Color(240, 125, 129));
                    } else if (data[i - 1][j - 1] == "   ") { //segundas horas das teóricas
                        p_celula.setBackground(new Color(251, 254, 112));
                    }
                    JLabel celula = new JLabel(data[i - 1][j - 1]);
                    p_celula.add(celula);
                    p_horario.add(p_celula);
                }
            }
        }

        p.add(p_horario, BorderLayout.NORTH);
        p.add(p2, BorderLayout.SOUTH);
        c.add(p);

        TrataEvento t = new TrataEvento();
        cancelar.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == cancelar) {
                dispose();
            }
        }
    }
}
