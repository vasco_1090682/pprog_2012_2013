package horarioproject;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;
import java.util.ArrayList;

public class ApresentarHorarioUI extends JFrame {

    private JComboBox cmbTurma, cmbDisciplina, cmbProfessor, cmbSala, cmbAluno;
    private JButton horarioAluno, horarioTurma, horarioDisciplina, horarioProfessor, horarioSala, cancelar;

    public ApresentarHorarioUI() {
        super("Apresentar Horários");

        Container c = getContentPane();

        JPanel p = new JPanel(new GridLayout(6, 1));
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("        Aluno:");
        cmbAluno = new JComboBox(Horario.NumerosTodosAlunos().toArray());
        cmbAluno.setPreferredSize(new Dimension(79, 25));
        cmbAluno.setSelectedIndex(-1);
        cmbAluno.setMaximumRowCount(10);
        horarioAluno = new JButton("Horário");
        p1.add(lb1);
        p1.add(cmbAluno);
        p1.add(horarioAluno);

        JPanel p2 = new JPanel();
        JLabel lb2 = new JLabel(" Disciplina:");
        cmbDisciplina = new JComboBox(Horario.SiglasTodasDisciplinas().toArray());
        cmbDisciplina.setPreferredSize(new Dimension(79, 25));
        cmbDisciplina.setSelectedIndex(-1);
        cmbDisciplina.setMaximumRowCount(10);
        horarioDisciplina = new JButton("Horário");
        p2.add(lb2);
        p2.add(cmbDisciplina);
        p2.add(horarioDisciplina);

        JPanel p3 = new JPanel();
        JLabel lb3 = new JLabel("Professor:");
        cmbProfessor = new JComboBox(Horario.SiglasTodosProfessores().toArray());
        cmbProfessor.setPreferredSize(new Dimension(79, 25));
        cmbProfessor.setSelectedIndex(-1);
        cmbProfessor.setMaximumRowCount(10);
        horarioProfessor = new JButton("Horário");
        p3.add(lb3);
        p3.add(cmbProfessor);
        p3.add(horarioProfessor);

        JPanel p4 = new JPanel();
        JLabel lb4 = new JLabel("           Sala:");
        cmbSala = new JComboBox(Horario.CodigosTodasSalas().toArray());
        cmbSala.setPreferredSize(new Dimension(79, 25));
        cmbSala.setSelectedIndex(-1);
        cmbSala.setMaximumRowCount(10);
        horarioSala = new JButton("Horário");
        p4.add(lb4);
        p4.add(cmbSala);
        p4.add(horarioSala);

        JPanel p5 = new JPanel();
        JLabel lb5 = new JLabel("       Turma:");
        cmbTurma = new JComboBox(Horario.DesignacoesTodasTurmas().toArray());
        cmbTurma.setPreferredSize(new Dimension(79, 25));
        cmbTurma.setSelectedIndex(-1);
        cmbTurma.setMaximumRowCount(10);
        horarioTurma = new JButton("Horário");
        p5.add(lb5);
        p5.add(cmbTurma);
        p5.add(horarioTurma);

        JPanel p6 = new JPanel();
        cancelar = new JButton("Cancelar");
        p6.add(cancelar);

        TrataEvento t = new TrataEvento();
        horarioAluno.addActionListener(t);
        horarioDisciplina.addActionListener(t);
        horarioProfessor.addActionListener(t);
        horarioSala.addActionListener(t);
        horarioTurma.addActionListener(t);
        cancelar.addActionListener(t);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        p.add(p6);
        c.add(p);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == horarioAluno) {
                try {
                    int numeroAluno = Integer.parseInt((String) (cmbAluno.getSelectedItem()));
                    ArrayList<CelulaHorario> horario = Horario.HorarioTurma(Horario.RetornarTurmaDoAluno(numeroAluno));
                    String mat[][] = Horario.PreencherMatrizHorario(horario);
                    String titulo = "Aluno " + numeroAluno;
                    HorarioUI h = new HorarioUI(mat, titulo);
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um aluno", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um aluno", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == horarioDisciplina) {
                try {
                    String siglaDisciplina = (String) (cmbDisciplina.getSelectedItem());
                    if (siglaDisciplina.isEmpty()) {
                        throw new DadoInvalidoException("Selecione uma disciplina");
                    }
                    ArrayList<CelulaHorario> horario = Horario.HorarioDisciplina(siglaDisciplina);
                    String mat[][] = Horario.PreencherMatrizHorario(horario);
                    String titulo = "Disciplina " + siglaDisciplina;
                    HorarioUI h = new HorarioUI(mat, titulo);
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma disciplina", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma disciplina", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == horarioProfessor) {
                try {
                    String siglaProfessor = (String) (cmbProfessor.getSelectedItem());
                    if (siglaProfessor.isEmpty()) {
                        throw new DadoInvalidoException("Selecione um professor");
                    }
                    ArrayList<CelulaHorario> horario = Horario.HorarioProfessor(siglaProfessor);
                    String mat[][] = Horario.PreencherMatrizHorario(horario);
                    String titulo = "Professor " + siglaProfessor;
                    HorarioUI h = new HorarioUI(mat, titulo);
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um professor", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um professor", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == horarioSala) {
                try {
                    String codigoSala = (String) (cmbSala.getSelectedItem());
                    if (codigoSala.isEmpty()) {
                        throw new DadoInvalidoException("Selecione uma sala");
                    }
                    ArrayList<CelulaHorario> horario = Horario.HorarioSala(codigoSala);
                    String mat[][] = Horario.PreencherMatrizHorario(horario);
                    String titulo = "Sala " + codigoSala;
                    HorarioUI h = new HorarioUI(mat, titulo);
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma sala", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma sala", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == horarioTurma) {
                try {
                    String designacaoTurma = (String) (cmbTurma.getSelectedItem());
                    if (designacaoTurma.isEmpty()) {
                        throw new DadoInvalidoException("Selecione uma turma");
                    }
                    ArrayList<CelulaHorario> horario = Horario.HorarioTurma(designacaoTurma);
                    String mat[][] = Horario.PreencherMatrizHorario(horario);
                    String titulo = "Turma " + designacaoTurma;
                    HorarioUI h = new HorarioUI(mat, titulo);
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
