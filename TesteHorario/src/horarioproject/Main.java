package horarioproject;

import horario.*;
import java.io.*;
import javax.swing.JOptionPane;

public class Main {

    public static void main(String[] args) {
        try {
            Horario.FicheirosALerNoInicio();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }

        Janela j = new Janela();
    }
}
