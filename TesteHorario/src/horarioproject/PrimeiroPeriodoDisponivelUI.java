package horarioproject;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class PrimeiroPeriodoDisponivelUI extends JDialog{
    private JButton encontrar, cancelar;
    private JComboBox cmbS, cmbD, cmbH;
    private JTextField txfHoraInicio, txfDuracao;

    public PrimeiroPeriodoDisponivelUI(JFrame framePai) {
        super(framePai, "Encontrar primeiro periodo disponível de uma sala", true);

        Container c = getContentPane();
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel(new GridLayout(3, 1));
        p1.setBorder(BorderFactory.createTitledBorder("Escolher sala"));

        JPanel p1_1 = new JPanel();
        JLabel lb1 = new JLabel("                   Sala:");
        cmbS = new JComboBox(Horario.CodigosTodasSalas().toArray());
        cmbS.setPreferredSize(new Dimension(75, 25));
        cmbS.setSelectedIndex(-1);
        cmbS.setMaximumRowCount(10);
        p1_1.add(lb1);
        p1_1.add(cmbS);

        JPanel p1_2 = new JPanel();
        JLabel lb2 = new JLabel("Dia da semana:");
        cmbD = new JComboBox(Horario.getDiasDaSemana());
        cmbD.setPreferredSize(new Dimension(75, 25));
        cmbD.setSelectedIndex(-1);
        cmbD.setMaximumRowCount(10);
        p1_2.add(lb2);
        p1_2.add(cmbD);

        JPanel p1_3 = new JPanel();
        JLabel lb3 = new JLabel("        Hora início:");
        cmbH = new JComboBox(Horario.getHorasInicio());
        cmbH.setPreferredSize(new Dimension(75, 25));
        cmbH.setSelectedIndex(-1);
        cmbH.setMaximumRowCount(10);
        p1_3.add(lb3);
        p1_3.add(cmbH);

        p1.add(p1_1);
        p1.add(p1_2);
        p1.add(p1_3);


        JPanel p2 = new JPanel(new GridLayout(3, 1));
        p2.setBorder(BorderFactory.createTitledBorder("Disponibilidade"));

        JPanel p2_1 = new JPanel();
        JLabel lb4 = new JLabel("        Hora início:");
        txfHoraInicio = new JTextField();
        txfHoraInicio.setPreferredSize(new Dimension(75, 25));
        txfHoraInicio.setEnabled(false);
        p2_1.add(lb4);
        p2_1.add(txfHoraInicio);

        JPanel p2_2 = new JPanel();
        JLabel lb5 = new JLabel("            Duração:");
        txfDuracao = new JTextField();
        txfDuracao.setPreferredSize(new Dimension(75, 25));
        txfDuracao.setEnabled(false);
        p2_2.add(lb5);
        p2_2.add(txfDuracao);

        p2.add(p2_1);
        p2.add(p2_2);

        JPanel p3 = new JPanel();
        cancelar = new JButton("Cancelar");
        encontrar = new JButton("Encontrar");
        p3.add(encontrar);
        p3.add(cancelar);        

        p.add(p1, BorderLayout.WEST);
        p.add(p2, BorderLayout.EAST);
        p.add(p3, BorderLayout.SOUTH);

        c.add(p);

        TrataEvento t = new TrataEvento();
        encontrar.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == encontrar) {
                try {
                    String codigoSala = (String) (cmbS.getSelectedItem());
                    int diaSemana = Integer.parseInt((String) (cmbD.getSelectedItem()));
                    int horaInicio = Integer.parseInt((String) (cmbH.getSelectedItem()));

                    int[] disp = Horario.EncontrarPeriodoDisponivel(codigoSala, diaSemana, horaInicio);
                    
                    txfHoraInicio.setText("" + disp[0]);
                    txfDuracao.setText("" + disp[1]);
                    
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
