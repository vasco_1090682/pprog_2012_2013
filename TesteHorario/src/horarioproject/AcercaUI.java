package horarioproject;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;
import java.util.ArrayList;

public class AcercaUI extends JDialog{
    private JButton btOk;
    
    public AcercaUI (JFrame framePai){
        super(framePai, "Acerca", true);
        
        Container c = getContentPane();
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        JPanel p1 = new JPanel(new GridLayout(2,1));
        p1.setBorder(BorderFactory.createTitledBorder("Autores"));
        JLabel lb1 = new JLabel("Eliana Gomes - 1090564");
        JLabel lb2 = new JLabel("Vasco Ribeiro - 1090682");
        p1.add(lb1);
        p1.add(lb2);
        
        JPanel p2 = new JPanel(new GridLayout(3, 1));
        JLabel lb3 = new JLabel("Trabalho PPROG 2012/2013");
        lb3.setHorizontalAlignment(JLabel.CENTER);
        JLabel lb4 = new JLabel("@ISEP");
        lb4.setHorizontalAlignment(JLabel.CENTER);
        p2.add(lb3);
        p2.add(lb4);
        
        JPanel p2_1 = new JPanel();
        btOk = new JButton("Ok");
        p2_1.add(btOk);
        p2.add(p2_1);
        
        
        p.add(p1, BorderLayout.NORTH);
        p.add(p2, BorderLayout.SOUTH);

        c.add(p);
        
        TrataEvento t = new TrataEvento();
        btOk.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }
    
    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btOk) {
               dispose();
            }
        }
    }
}
