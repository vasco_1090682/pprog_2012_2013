package horarioproject;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import cargahoraria.*;
import listagens.*;
import mostrarDados.*;
import celulaTurma.*;
import utilitarios.Data;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import horario.*;
import java.io.*;

public class Janela extends JFrame {
    
    public Janela()  {
        super("Gestão de Horários");
        Container c = getContentPane();
        
        //relógio
        final JLabel lbTempo = new JLabel();
        final DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");  
        ActionListener timerListener = new ActionListener()  
        {  
            public void actionPerformed(ActionEvent e)  
            {  
                Date date = new Date();  
                String time = timeFormat.format(date);  
                lbTempo.setText(time);  
            }  
        };  
        Timer timer = new Timer(1000, timerListener);
        timer.setInitialDelay(0);  
        timer.start();
        
        //data
        Calendar hoje = Calendar.getInstance();
        int ano = hoje.get(Calendar.YEAR);
        int mes = hoje.get(Calendar.MONTH) + 1;
        int dia = hoje.get(Calendar.DAY_OF_MONTH);
        Data d = new Data(ano, mes, dia);
        JLabel lbData = new JLabel();
        lbData.setText(" -  " +d.toString());
        
        JPanel tempoData = new JPanel();        
        tempoData.setBackground(new Color(115, 138, 12));
        lbTempo.setFont(new Font("Arial", Font.BOLD, 28));
        lbTempo.setForeground(Color.white);
        lbData.setFont(new Font("Arial", Font.BOLD, 28));
        lbData.setForeground(Color.white);
        tempoData.add(lbTempo);
        tempoData.add(lbData);
        
        //fundo
        PainelFundo pf = new PainelFundo(); 
        c.add(pf, BorderLayout.CENTER);
        
        c.add(tempoData, BorderLayout.SOUTH);
        
        
        
        //menu
        JMenuBar menuBar = new JMenuBar();
        JMenu menu;
        JMenuItem menuItem;
        JMenu subMenu;
        
        //----------Menu Gerir----------
        menu = new JMenu("Gerir");
        menu.setMnemonic('G');
        menuBar.add(menu);
        
        //----------submenu turma----------
        subMenu = new JMenu("Turma");
        subMenu.setMnemonic('T');
        menu.add(subMenu);
        
        menuItem = new JMenuItem("Inserir célula", 'I');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl I"));
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {             
                    InserirCelulaUI icui = new InserirCelulaUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Editar célula", 'E');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {             
                    EditarCelulaUI ecui = new EditarCelulaUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Apagar célula", 'A');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl A"));
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {             
                    ApagarCelulaUI acui = new ApagarCelulaUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        //----------fim submenu turma----------
        
        menu.addSeparator();
        menuItem = new JMenuItem("Carregar ficheiros originais", 'F');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl F"));
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CarregarFicheirosOriginais();
            }
        });
        menu.add(menuItem);
        
        menu.addSeparator();
        menuItem = new JMenuItem("Sair", 'S');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fechar();
            }
        });
        menu.add(menuItem);
        //----------fim Menu Gerir----------
        
        //----------Menu Ver----------
        menu = new JMenu("Ver");
        menu.setMnemonic('V');
        menuBar.add(menu);
        
        //----------submenu carga horário----------
        subMenu = new JMenu("Carga horária");
        subMenu.setMnemonic('C');
        menu.add(subMenu);
        
        menuItem = new JMenuItem("Aluno", 'A');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   CargaHorariaAlunoUI chaui = new CargaHorariaAlunoUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Disciplina", 'D');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   CargaHorariaDisciplinaUI chdui = new CargaHorariaDisciplinaUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Professor", 'P');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   CargaHorariaProfessorUI chpui = new CargaHorariaProfessorUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Sala", 'S');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   CargaHorariaSalaUI chsui = new CargaHorariaSalaUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Turma", 'T');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   CargaHorariaTurmaUI chtui = new CargaHorariaTurmaUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        //----------fim submenu carga horário----------
        
        
        menuItem = new JMenuItem("Horário", 'H');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl H"));
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                ApresentarHorarioUI a = new ApresentarHorarioUI();  
            }
        });
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Primeiro período disponível", 'R');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl P"));
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   PrimeiroPeriodoDisponivelUI ppdui = new PrimeiroPeriodoDisponivelUI(Janela.this);
            }
        });
        menu.add(menuItem);
        
        //----------submenu dados----------
        subMenu = new JMenu("Dados");
        subMenu.setMnemonic('O');
        menu.add(subMenu);
        
        menuItem = new JMenuItem("Aluno", 'A');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   DadosAlunoUI daui = new DadosAlunoUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Disciplina", 'D');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   DadosDisciplinaUI ddui = new DadosDisciplinaUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Professor", 'P');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   DadosProfessorUI dpui = new DadosProfessorUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Sala", 'S');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   DadosSalaUI dsui = new DadosSalaUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        
        menuItem = new JMenuItem("Turma", 'T');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   DadosTurmaUI dtui = new DadosTurmaUI(Janela.this);
            }
        });
        subMenu.add(menuItem);
        //----------fim submenu dados----------
        //----------fim Menu Ver----------
        
        //----------Menu Listagens----------
        menu = new JMenu("Listagens");
        menu.setMnemonic('L');
        menuBar.add(menu);
        
        menuItem = new JMenuItem("Alunos de uma turma ordenados", 'U');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   AlunosTurmaOrdenadosUI a = new AlunosTurmaOrdenadosUI(Janela.this);
            }
        });
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Salas por taxa de ocupação", 'X');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                   SalasTaxaOcupacaoUI s = new SalasTaxaOcupacaoUI(Janela.this);
            }
        });
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Disciplinas por professor(turmas)", 'N');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                  DisciplinasLecionadasProfessorUI d = new DisciplinasLecionadasProfessorUI(Janela.this);
            }
        });
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Disciplinas por sala", 'I');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                  DisciplinasPorSalaUI d = new DisciplinasPorSalaUI (Janela.this);
            }
        });
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Professores por turma(disciplinas/tipo aula)", 'F');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                  ProfessoresPorTurmaUI p = new ProfessoresPorTurmaUI (Janela.this);
            }
        });
        menu.add(menuItem);
        //----------fim Menu Listagens----------
        
        //----------Menu Ajuda----------
        menu = new JMenu("Ajuda");
        menu.setMnemonic('J');
        menuBar.add(menu);
        
        menuItem = new JMenuItem("Acerca", 'A');
        menuItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {             
                  AcercaUI p = new AcercaUI(Janela.this);
            }
        });
        menu.add(menuItem);
        //----------fim Menu Ajuda----------
        
        setJMenuBar(menuBar);
        
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setSize(500, 300);
        setMinimumSize(new Dimension(700, 500));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }
    
    private void fechar() {
        Object[] opSimNao = {"Sim", "Não"};
        if (JOptionPane.showOptionDialog(Janela.this, "Deseja fechar a aplicação?", "Gestão de Horários", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opSimNao, opSimNao[1]) == JOptionPane.YES_OPTION) {
            try {
                Horario.gravarEstadoActual();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
            dispose();
        }
    }
    
    private class PainelFundo extends JPanel {

        public void paintComponent(Graphics g) { // método reescrito; desenha componentes do painel 
            super.paintComponent(g);
            Dimension dimensaoPainel = getSize(); // para redimensionar imagem à medida do painel 
            double largura = dimensaoPainel.getWidth();
            double altura = dimensaoPainel.getHeight();
            ImageIcon i1 = new ImageIcon("green-gradient.png"); // imagem guardada na pasta do aplicação 
            Image i2 = i1.getImage().getScaledInstance((int) largura, (int) altura, Image.SCALE_SMOOTH);
            Image i3 = new ImageIcon(i2).getImage();
            g.drawImage(i3, 0, 0, this);
        }
    }
    
    private void CarregarFicheirosOriginais() {
        try {
            Horario.CarregarFicheirosOriginais();
            JOptionPane.showMessageDialog(null, "Ficheiros originais carregados com sucesso", "Aviso", JOptionPane.INFORMATION_MESSAGE);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
}
