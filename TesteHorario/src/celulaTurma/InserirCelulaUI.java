package celulaTurma;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class InserirCelulaUI extends JDialog {
    private JComboBox cmbTurma, cmbDisciplina, cmbTipoAula, cmbDiaSemana, cmbHoraInicio, cmbDuracao, cmbProfessor, cmbSala;
    private JButton inserir, cancelar;

    public InserirCelulaUI(JFrame framePai) {
        super(framePai, "Inserir célula num horário", true);

        Container c = getContentPane();
        JPanel p = new JPanel(new GridLayout(8, 1));
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("                Turma:");
        cmbTurma = new JComboBox(Horario.DesignacoesTodasTurmas().toArray());
        cmbTurma.setPreferredSize(new Dimension(75, 25));
        cmbTurma.setSelectedIndex(-1);
        cmbTurma.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbTurma);

        JPanel p2 = new JPanel();
        JLabel lb2 = new JLabel("          Disciplina:");
        cmbDisciplina = new JComboBox(Horario.SiglasTodasDisciplinas().toArray());
        cmbDisciplina.setPreferredSize(new Dimension(75, 25));
        cmbDisciplina.setSelectedIndex(-1);
        cmbDisciplina.setMaximumRowCount(10);
        p2.add(lb2);
        p2.add(cmbDisciplina);

        JPanel p3 = new JPanel();
        JLabel lb3 = new JLabel("           Tipo aula:");
        cmbTipoAula = new JComboBox(CelulaHorario.TipoAula.values());
        cmbTipoAula.setPreferredSize(new Dimension(75, 25));
        cmbTipoAula.setSelectedIndex(-1);
        cmbTipoAula.setMaximumRowCount(10);
        p3.add(lb3);
        p3.add(cmbTipoAula);

        JPanel p4 = new JPanel();
        JLabel lb4 = new JLabel("Dia da semana:");
        cmbDiaSemana = new JComboBox(Horario.getDiasDaSemana());
        cmbDiaSemana.setPreferredSize(new Dimension(75, 25));
        cmbDiaSemana.setSelectedIndex(-1);
        cmbDiaSemana.setMaximumRowCount(10);
        p4.add(lb4);
        p4.add(cmbDiaSemana);

        JPanel p5 = new JPanel();
        JLabel lb5 = new JLabel("        Hora início:");
        cmbHoraInicio = new JComboBox(Horario.getHorasInicio());
        cmbHoraInicio.setPreferredSize(new Dimension(75, 25));
        cmbHoraInicio.setSelectedIndex(-1);
        cmbHoraInicio.setMaximumRowCount(10);
        p5.add(lb5);
        p5.add(cmbHoraInicio);

        JPanel p6 = new JPanel();
        JLabel lb6 = new JLabel("            Duração:");
        cmbDuracao = new JComboBox(Horario.getDuracoes());
        cmbDuracao.setPreferredSize(new Dimension(75, 25));
        cmbDuracao.setSelectedIndex(-1);
        cmbDuracao.setMaximumRowCount(10);
        p6.add(lb6);
        p6.add(cmbDuracao);

        JPanel p7 = new JPanel();
        JLabel lb7 = new JLabel("         Professor:");
        cmbProfessor = new JComboBox(Horario.SiglasTodosProfessores().toArray());
        cmbProfessor.setPreferredSize(new Dimension(75, 25));
        cmbProfessor.setSelectedIndex(-1);
        cmbProfessor.setMaximumRowCount(10);
        p7.add(lb7);
        p7.add(cmbProfessor);

        JPanel p8 = new JPanel();
        JLabel lb8 = new JLabel("                    Sala:");
        cmbSala = new JComboBox(Horario.CodigosTodasSalas().toArray());
        cmbSala.setPreferredSize(new Dimension(75, 25));
        cmbSala.setSelectedIndex(-1);
        cmbSala.setMaximumRowCount(10);
        p8.add(lb8);
        p8.add(cmbSala);

        JPanel p9 = new JPanel();
        inserir = new JButton("Inserir");
        cancelar = new JButton("Cancelar");
        p9.add(inserir);
        p9.add(cancelar);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        p.add(p6);
        p.add(p7);
        p.add(p8);

        c.add(p, BorderLayout.CENTER);
        c.add(p9, BorderLayout.SOUTH);

        TrataEvento t = new TrataEvento();
        inserir.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }
    
    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == inserir) {
                try {
                    String designacaoTurma = (String) (cmbTurma.getSelectedItem());
                    String siglaDisciplina = (String) (cmbDisciplina.getSelectedItem());
                    String tipoAula = cmbTipoAula.getSelectedItem().toString();
                    int diaSemana = Integer.parseInt((String) (cmbDiaSemana.getSelectedItem()));
                    int horaInicio = Integer.parseInt((String) (cmbHoraInicio.getSelectedItem()));
                    int duracao = Integer.parseInt((String) (cmbDuracao.getSelectedItem()));
                    String siglaProfessor = (String) (cmbProfessor.getSelectedItem());
                    String codigoSala = (String) (cmbSala.getSelectedItem());

                    Horario.InserirCelulaTurma(designacaoTurma, siglaDisciplina, tipoAula, diaSemana, horaInicio, duracao, siglaProfessor, codigoSala);

                    Object[] opSimNao = {"Sim", "Não"};
                    if (JOptionPane.showOptionDialog(InserirCelulaUI.this, "Célula inserida. Deseja inserir outra célula?", "Sucesso", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opSimNao, opSimNao[1]) == JOptionPane.NO_OPTION) {
                        dispose();
                    } else {
                        //reset as combo boxs se for para outra célula
                        cmbTurma.setSelectedIndex(-1);
                        cmbDisciplina.setSelectedIndex(-1);
                        cmbTipoAula.setSelectedIndex(-1);
                        cmbDiaSemana.setSelectedIndex(-1);
                        cmbHoraInicio.setSelectedIndex(-1);
                        cmbDuracao.setSelectedIndex(-1);
                        cmbProfessor.setSelectedIndex(-1);
                        cmbSala.setSelectedIndex(-1);
                    }
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}