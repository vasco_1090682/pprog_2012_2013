package celulaTurma;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class ApagarCelulaUI extends JDialog {

    private JButton escolher, apagar, cancelar;
    private JComboBox cmbT, cmbD, cmbH;
    private JTextField txfTurma, txfDisciplina, txfTipoAula, txfDiaSemana, txfHoraInicio, txfDuracao, txfProfessor, txfSala;
    CelulaHorario ch;

    public ApagarCelulaUI(JFrame framePai) {
        super(framePai, "Apagar célula de um horário de uma turma", true);

        Container c = getContentPane();
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel(new GridLayout(9, 1));
        p1.setBorder(BorderFactory.createTitledBorder("Escolher célula a apagar"));

        JPanel p1_1 = new JPanel();
        JLabel lb1 = new JLabel("                Turma:");
        cmbT = new JComboBox(Horario.DesignacoesTodasTurmas().toArray());
        cmbT.setPreferredSize(new Dimension(75, 25));
        cmbT.setSelectedIndex(-1);
        cmbT.setMaximumRowCount(10);
        p1_1.add(lb1);
        p1_1.add(cmbT);

        JPanel p1_2 = new JPanel();
        JLabel lb2 = new JLabel("Dia da semana:");
        cmbD = new JComboBox(Horario.getDiasDaSemana());
        cmbD.setPreferredSize(new Dimension(75, 25));
        cmbD.setSelectedIndex(-1);
        cmbD.setMaximumRowCount(10);
        p1_2.add(lb2);
        p1_2.add(cmbD);

        JPanel p1_3 = new JPanel();
        JLabel lb3 = new JLabel("        Hora início:");
        cmbH = new JComboBox(Horario.getHorasInicio());
        cmbH.setPreferredSize(new Dimension(75, 25));
        cmbH.setSelectedIndex(-1);
        cmbH.setMaximumRowCount(10);
        p1_3.add(lb3);
        p1_3.add(cmbH);

        JPanel p1_4 = new JPanel();
        escolher = new JButton("Escolher célula");
        p1_4.add(escolher);

        p1.add(p1_1);
        p1.add(p1_2);
        p1.add(p1_3);
        p1.add(p1_4);


        JPanel p2 = new JPanel(new GridLayout(9, 1));
        p2.setBorder(BorderFactory.createTitledBorder("Célula a apagar"));

        JPanel p2_1 = new JPanel();
        JLabel lb4 = new JLabel("                Turma:");
        txfTurma = new JTextField();
        txfTurma.setPreferredSize(new Dimension(75, 25));
        txfTurma.setEnabled(false);
        p2_1.add(lb4);
        p2_1.add(txfTurma);

        JPanel p2_2 = new JPanel();
        JLabel lb5 = new JLabel("          Disciplina:");
        txfDisciplina = new JTextField();
        txfDisciplina.setPreferredSize(new Dimension(75, 25));
        txfDisciplina.setEnabled(false);
        p2_2.add(lb5);
        p2_2.add(txfDisciplina);

        JPanel p2_3 = new JPanel();
        JLabel lb6 = new JLabel("           Tipo aula:");
        txfTipoAula = new JTextField();
        txfTipoAula.setPreferredSize(new Dimension(75, 25));
        txfTipoAula.setEnabled(false);
        p2_3.add(lb6);
        p2_3.add(txfTipoAula);

        JPanel p2_4 = new JPanel();
        JLabel lb7 = new JLabel("Dia da semana:");
        txfDiaSemana = new JTextField();
        txfDiaSemana.setPreferredSize(new Dimension(75, 25));
        txfDiaSemana.setEnabled(false);
        p2_4.add(lb7);
        p2_4.add(txfDiaSemana);

        JPanel p2_5 = new JPanel();
        JLabel lb8 = new JLabel("        Hora início:");
        txfHoraInicio = new JTextField();
        txfHoraInicio.setPreferredSize(new Dimension(75, 25));
        txfHoraInicio.setEnabled(false);
        p2_5.add(lb8);
        p2_5.add(txfHoraInicio);

        JPanel p2_6 = new JPanel();
        JLabel lb9 = new JLabel("            Duração:");
        txfDuracao = new JTextField();
        txfDuracao.setPreferredSize(new Dimension(75, 25));
        txfDuracao.setEnabled(false);
        p2_6.add(lb9);
        p2_6.add(txfDuracao);

        JPanel p2_7 = new JPanel();
        JLabel lb10 = new JLabel("         Professor:");
        txfProfessor = new JTextField();
        txfProfessor.setPreferredSize(new Dimension(75, 25));
        txfProfessor.setEnabled(false);
        p2_7.add(lb10);
        p2_7.add(txfProfessor);

        JPanel p2_8 = new JPanel();
        JLabel lb11 = new JLabel("                    Sala:");
        txfSala = new JTextField();
        txfSala.setPreferredSize(new Dimension(75, 25));
        txfSala.setEnabled(false);
        p2_8.add(lb11);
        p2_8.add(txfSala);

        JPanel p2_9 = new JPanel();
        apagar = new JButton("Apagar");
        apagar.setEnabled(false);
        p2_9.add(apagar);

        p2.add(p2_1);
        p2.add(p2_2);
        p2.add(p2_3);
        p2.add(p2_4);
        p2.add(p2_5);
        p2.add(p2_6);
        p2.add(p2_7);
        p2.add(p2_8);
        p2.add(p2_9);

        JPanel p3 = new JPanel();
        cancelar = new JButton("Cancelar");
        p3.add(cancelar);

        p.add(p1, BorderLayout.WEST);
        p.add(p2, BorderLayout.EAST);
        p.add(p3, BorderLayout.SOUTH);

        c.add(p);

        TrataEvento t = new TrataEvento();
        escolher.addActionListener(t);
        apagar.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == escolher) {
                try {
                    String designacaoTurma = (String) (cmbT.getSelectedItem());
                    int diaSemana = Integer.parseInt((String) (cmbD.getSelectedItem()));
                    int horaInicio = Integer.parseInt((String) (cmbH.getSelectedItem()));

                    ch = Horario.RetornarCelulaPelaTurmaDiaHora(designacaoTurma, diaSemana, horaInicio);

                    apagar.setEnabled(true);

                    txfTurma.setText(ch.getTurma().getDesignacao());
                    txfDisciplina.setText(ch.getDisciplina().getSigla());
                    txfTipoAula.setText(ch.getTipoAula().toString());
                    txfDiaSemana.setText("" + ch.getDiaSemana());
                    txfHoraInicio.setText("" + ch.getHoraInicio());
                    txfDuracao.setText("" + ch.getDuracao());
                    txfProfessor.setText(ch.getProfessor().getSigla());
                    txfSala.setText(ch.getSala().getCodigo());

                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                    //reset as combo boxs se existir célula
                    txfTurma.setText("");
                    txfDisciplina.setText("");
                    txfTipoAula.setText("");
                    txfDiaSemana.setText("");
                    txfHoraInicio.setText("");
                    txfDuracao.setText("");
                    txfProfessor.setText("");
                    txfSala.setText("");
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == apagar) {
                try {
                    Horario.EliminarCelulaTurma(ch.getTurma().getDesignacao(), ch.getDiaSemana(), ch.getHoraInicio());
                    
                    Object[] opSimNao = {"Sim", "Não"};
                    if (JOptionPane.showOptionDialog(ApagarCelulaUI.this, "Célula apagada. Deseja apagar outra célula?", "Sucesso", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opSimNao, opSimNao[1]) == JOptionPane.NO_OPTION) {
                        dispose();
                    } else {
                        //reset as combo boxs e os textfiels se for para outra célula
                        txfTurma.setText("");
                        txfDisciplina.setText("");
                        txfTipoAula.setText("");
                        txfDiaSemana.setText("");
                        txfHoraInicio.setText("");
                        txfDuracao.setText("");
                        txfProfessor.setText("");
                        txfSala.setText("");
                        cmbT.setSelectedIndex(-1);
                        cmbD.setSelectedIndex(-1);
                        cmbH.setSelectedIndex(-1);
                    }
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
