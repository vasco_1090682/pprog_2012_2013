package celulaTurma;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class EditarCelulaUI extends JDialog {

    private JButton escolher, editar, reset, cancelar;
    private JComboBox cmbT, cmbD, cmbH;
    private JComboBox cmbTurma, cmbDisciplina, cmbTipoAula, cmbDiaSemana, cmbHoraInicio, cmbDuracao, cmbProfessor, cmbSala;
    CelulaHorario ch;

    public EditarCelulaUI(JFrame framePai) {
        super(framePai, "Editar célula de um horário de uma turma", true);

        Container c = getContentPane();
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));



        JPanel p1 = new JPanel(new GridLayout(9, 1));
        p1.setBorder(BorderFactory.createTitledBorder("Escolher célula a editar"));

        JPanel p1_1 = new JPanel();
        JLabel lb1 = new JLabel("                Turma:");
        cmbT = new JComboBox(Horario.DesignacoesTodasTurmas().toArray());
        cmbT.setPreferredSize(new Dimension(75, 25));
        cmbT.setSelectedIndex(-1);
        cmbT.setMaximumRowCount(10);
        p1_1.add(lb1);
        p1_1.add(cmbT);

        JPanel p1_2 = new JPanel();
        JLabel lb2 = new JLabel("Dia da semana:");
        cmbD = new JComboBox(Horario.getDiasDaSemana());
        cmbD.setPreferredSize(new Dimension(75, 25));
        cmbD.setSelectedIndex(-1);
        cmbD.setMaximumRowCount(10);
        p1_2.add(lb2);
        p1_2.add(cmbD);

        JPanel p1_3 = new JPanel();
        JLabel lb3 = new JLabel("        Hora início:");
        cmbH = new JComboBox(Horario.getHorasInicio());
        cmbH.setPreferredSize(new Dimension(75, 25));
        cmbH.setSelectedIndex(-1);
        cmbH.setMaximumRowCount(10);
        p1_3.add(lb3);
        p1_3.add(cmbH);

        JPanel p1_4 = new JPanel();
        escolher = new JButton("Escolher célula");
        p1_4.add(escolher);

        p1.add(p1_1);
        p1.add(p1_2);
        p1.add(p1_3);
        p1.add(p1_4);


        JPanel p2 = new JPanel(new GridLayout(9, 1));
        p2.setBorder(BorderFactory.createTitledBorder("Editar célula"));

        JPanel p2_1 = new JPanel();
        JLabel lb4 = new JLabel("                Turma:");
        cmbTurma = new JComboBox(Horario.DesignacoesTodasTurmas().toArray());
        cmbTurma.setPreferredSize(new Dimension(75, 25));
        cmbTurma.setSelectedIndex(-1);
        cmbTurma.setMaximumRowCount(10);
        cmbTurma.setEnabled(false);
        p2_1.add(lb4);
        p2_1.add(cmbTurma);

        JPanel p2_2 = new JPanel();
        JLabel lb5 = new JLabel("          Disciplina:");
        cmbDisciplina = new JComboBox(Horario.SiglasTodasDisciplinas().toArray());
        cmbDisciplina.setPreferredSize(new Dimension(75, 25));
        cmbDisciplina.setSelectedIndex(-1);
        cmbDisciplina.setMaximumRowCount(10);
        cmbDisciplina.setEnabled(false);
        p2_2.add(lb5);
        p2_2.add(cmbDisciplina);

        JPanel p2_3 = new JPanel();
        JLabel lb6 = new JLabel("           Tipo aula:");
        cmbTipoAula = new JComboBox(CelulaHorario.TipoAula.values());
        cmbTipoAula.setPreferredSize(new Dimension(75, 25));
        cmbTipoAula.setSelectedIndex(-1);
        cmbTipoAula.setMaximumRowCount(10);
        cmbTipoAula.setEnabled(false);
        p2_3.add(lb6);
        p2_3.add(cmbTipoAula);

        JPanel p2_4 = new JPanel();
        JLabel lb7 = new JLabel("Dia da semana:");
        cmbDiaSemana = new JComboBox(Horario.getDiasDaSemana());
        cmbDiaSemana.setPreferredSize(new Dimension(75, 25));
        cmbDiaSemana.setSelectedIndex(-1);
        cmbDiaSemana.setMaximumRowCount(10);
        cmbDiaSemana.setEnabled(false);
        p2_4.add(lb7);
        p2_4.add(cmbDiaSemana);

        JPanel p2_5 = new JPanel();
        JLabel lb8 = new JLabel("        Hora início:");
        cmbHoraInicio = new JComboBox(Horario.getHorasInicio());
        cmbHoraInicio.setPreferredSize(new Dimension(75, 25));
        cmbHoraInicio.setSelectedIndex(-1);
        cmbHoraInicio.setMaximumRowCount(10);
        cmbHoraInicio.setEnabled(false);
        p2_5.add(lb8);
        p2_5.add(cmbHoraInicio);

        JPanel p2_6 = new JPanel();
        JLabel lb9 = new JLabel("            Duração:");
        cmbDuracao = new JComboBox(Horario.getDuracoes());
        cmbDuracao.setPreferredSize(new Dimension(75, 25));
        cmbDuracao.setSelectedIndex(-1);
        cmbDuracao.setMaximumRowCount(10);
        cmbDuracao.setEnabled(false);
        p2_6.add(lb9);
        p2_6.add(cmbDuracao);

        JPanel p2_7 = new JPanel();
        JLabel lb10 = new JLabel("         Professor:");
        cmbProfessor = new JComboBox(Horario.SiglasTodosProfessores().toArray());
        cmbProfessor.setPreferredSize(new Dimension(75, 25));
        cmbProfessor.setSelectedIndex(-1);
        cmbProfessor.setMaximumRowCount(10);
        cmbProfessor.setEnabled(false);
        p2_7.add(lb10);
        p2_7.add(cmbProfessor);

        JPanel p2_8 = new JPanel();
        JLabel lb11 = new JLabel("                    Sala:");
        cmbSala = new JComboBox(Horario.CodigosTodasSalas().toArray());
        cmbSala.setPreferredSize(new Dimension(75, 25));
        cmbSala.setSelectedIndex(-1);
        cmbSala.setMaximumRowCount(10);
        cmbSala.setEnabled(false);
        p2_8.add(lb11);
        p2_8.add(cmbSala);

        JPanel p2_9 = new JPanel();
        editar = new JButton("Editar");
        editar.setEnabled(false);
        reset = new JButton("Reset");
        reset.setEnabled(false);
        p2_9.add(editar);
        p2_9.add(reset);

        p2.add(p2_1);
        p2.add(p2_2);
        p2.add(p2_3);
        p2.add(p2_4);
        p2.add(p2_5);
        p2.add(p2_6);
        p2.add(p2_7);
        p2.add(p2_8);
        p2.add(p2_9);


        JPanel p3 = new JPanel();
        cancelar = new JButton("Cancelar");
        p3.add(cancelar);


        p.add(p1, BorderLayout.WEST);
        p.add(p2, BorderLayout.EAST);
        p.add(p3, BorderLayout.SOUTH);

        c.add(p);

        TrataEvento t = new TrataEvento();
        escolher.addActionListener(t);
        editar.addActionListener(t);
        reset.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == escolher) {
                try {
                    String designacaoTurma = (String) (cmbT.getSelectedItem());
                    int diaSemana = Integer.parseInt((String) (cmbD.getSelectedItem()));
                    int horaInicio = Integer.parseInt((String) (cmbH.getSelectedItem()));

                    ch = Horario.RetornarCelulaPelaTurmaDiaHora(designacaoTurma, diaSemana, horaInicio);

                    cmbDisciplina.setEnabled(true);
                    cmbTipoAula.setEnabled(true);
                    cmbDiaSemana.setEnabled(true);
                    cmbHoraInicio.setEnabled(true);
                    cmbDuracao.setEnabled(true);
                    cmbProfessor.setEnabled(true);
                    cmbSala.setEnabled(true);
                    editar.setEnabled(true);
                    reset.setEnabled(true);

                    cmbTurma.setSelectedItem(ch.getTurma().getDesignacao());
                    cmbDisciplina.setSelectedItem(ch.getDisciplina().getSigla());
                    cmbTipoAula.setSelectedItem(ch.getTipoAula());
                    cmbDiaSemana.setSelectedItem("" + ch.getDiaSemana());
                    cmbHoraInicio.setSelectedItem("" + ch.getHoraInicio());
                    cmbDuracao.setSelectedItem("" + ch.getDuracao());
                    cmbProfessor.setSelectedItem(ch.getProfessor().getSigla());
                    cmbSala.setSelectedItem(ch.getSala().getCodigo());

                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                    //reset as combo boxs se existir célula
                    cmbTurma.setSelectedIndex(-1);
                    cmbDisciplina.setSelectedIndex(-1);
                    cmbTipoAula.setSelectedIndex(-1);
                    cmbDiaSemana.setSelectedIndex(-1);
                    cmbHoraInicio.setSelectedIndex(-1);
                    cmbDuracao.setSelectedIndex(-1);
                    cmbProfessor.setSelectedIndex(-1);
                    cmbSala.setSelectedIndex(-1);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == editar) {
                try {
                    String t = ch.getTurma().getDesignacao();
                    String d = ch.getDisciplina().getSigla();
                    String ta = ch.getTipoAula().toString();
                    int ds = ch.getDiaSemana();
                    int hi = ch.getHoraInicio();
                    int dur = ch.getDuracao();
                    String p = ch.getProfessor().getSigla();
                    String s = ch.getSala().getCodigo();

                    String tNew = (String) (cmbTurma.getSelectedItem());
                    String dNew = (String) (cmbDisciplina.getSelectedItem());
                    String taNew = cmbTipoAula.getSelectedItem().toString();
                    int dsNew = Integer.parseInt((String) (cmbDiaSemana.getSelectedItem()));
                    int hiNew = Integer.parseInt((String) (cmbHoraInicio.getSelectedItem()));
                    int durNew = Integer.parseInt((String) (cmbDuracao.getSelectedItem()));
                    String pNew = (String) (cmbProfessor.getSelectedItem());
                    String sNew = (String) (cmbSala.getSelectedItem());

                    if (t.equals(tNew) && d.equals(dNew) && ta.equals(taNew) && ds == dsNew && hi == hiNew && dur == durNew && p.equals(pNew) && s.equals(sNew)) {
                        JOptionPane.showMessageDialog(null, "Não ocorreu nenhum alteração!", "Aviso", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        Horario.AlterarCelulaTurma(t, d, ta, ds, hi, dur, p, s, tNew, dNew, taNew, dsNew, hiNew, durNew, pNew, sNew);
                        Object[] opSimNao = {"Sim", "Não"};
                        if (JOptionPane.showOptionDialog(EditarCelulaUI.this, "Célula alterada. Deseja alterar outra célula?", "Sucesso", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opSimNao, opSimNao[1]) == JOptionPane.NO_OPTION) {
                            dispose();
                        } else {
                            //reset as combo boxs se for editar outra célula
                            cmbT.setSelectedIndex(-1);
                            cmbD.setSelectedIndex(-1);
                            cmbH.setSelectedIndex(-1);
                            cmbTurma.setSelectedIndex(-1);
                            cmbDisciplina.setSelectedIndex(-1);
                            cmbTipoAula.setSelectedIndex(-1);
                            cmbDiaSemana.setSelectedIndex(-1);
                            cmbHoraInicio.setSelectedIndex(-1);
                            cmbDuracao.setSelectedIndex(-1);
                            cmbProfessor.setSelectedIndex(-1);
                            cmbSala.setSelectedIndex(-1);

                            //volta a bloquear as boxs/buttons
                            cmbDisciplina.setEnabled(false);
                            cmbTipoAula.setEnabled(false);
                            cmbDiaSemana.setEnabled(false);
                            cmbHoraInicio.setEnabled(false);
                            cmbDuracao.setEnabled(false);
                            cmbProfessor.setEnabled(false);
                            cmbSala.setEnabled(false);
                            editar.setEnabled(false);
                            reset.setEnabled(false);
                        }
                    }
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Campos vazios", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == reset) {
                cmbTurma.setSelectedItem(ch.getTurma().getDesignacao());
                cmbDisciplina.setSelectedItem(ch.getDisciplina().getSigla());
                cmbTipoAula.setSelectedItem(ch.getTipoAula());
                cmbDiaSemana.setSelectedItem("" + ch.getDiaSemana());
                cmbHoraInicio.setSelectedItem("" + ch.getHoraInicio());
                cmbDuracao.setSelectedItem("" + ch.getDuracao());
                cmbProfessor.setSelectedItem(ch.getProfessor().getSigla());
                cmbSala.setSelectedItem(ch.getSala().getCodigo());
            } else {
                dispose();
            }
        }
    }
}
