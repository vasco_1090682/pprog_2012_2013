package cargahoraria;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class CargaHorariaProfessorUI extends JDialog {
    private JComboBox cmbProfessor;
    private JButton carga, cancelar;
    private JTextField txfCarga;
    
    public CargaHorariaProfessorUI(JFrame framePai){
        super(framePai, "Carga horária semanal - Professor", true);
        Container c = getContentPane();

        JPanel p = new JPanel(new GridLayout(3, 1));
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("       Professor:");
        cmbProfessor = new JComboBox(Horario.SiglasTodosProfessores().toArray());
        cmbProfessor.setPreferredSize(new Dimension(79, 25));
        cmbProfessor.setSelectedIndex(-1);
        cmbProfessor.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbProfessor);
        
        JPanel p2 = new JPanel();
        JLabel lb2 = new JLabel("Carga Horária:");
        txfCarga = new JTextField(7);
        txfCarga.setEnabled(false);
        p2.add(lb2);
        p2.add(txfCarga);
        
        JPanel p3 = new JPanel();
        carga = new JButton("Carga horária");
        cancelar = new JButton("Cancelar");
        p3.add(carga);
        p3.add(cancelar);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        
        c.add(p);

        TrataEvento t = new TrataEvento();
        carga.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }
    
    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == carga) {
                try {
                    String siglaProfessor = (String) (cmbProfessor.getSelectedItem());
                    int cargaHorariaProfessor = Horario.CargaHorariaSemanalProfessor(siglaProfessor);
                    txfCarga.setText("" + cargaHorariaProfessor);
                } catch (DadoInvalidoException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um professor", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um professor", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
