package listagens;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;
import java.util.ArrayList;

public class DisciplinasLecionadasProfessorUI extends JDialog {
    private JComboBox cmbProfessor;
    private JButton mostrarDisciplinas, cancelar;
    private JList listaDisciplinas;
    
    public DisciplinasLecionadasProfessorUI (JFrame framePai){
        super(framePai, "Listar disciplinas lecionadas por um professor", true);
        
        Container c = getContentPane();
        
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("Professor:");
        cmbProfessor = new JComboBox(Horario.SiglasTodosProfessores().toArray());
        cmbProfessor.setPreferredSize(new Dimension(79, 25));
        cmbProfessor.setSelectedIndex(-1);
        cmbProfessor.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbProfessor);

        JPanel p2 = new JPanel();

        JPanel p2_1 = new JPanel();
        JLabel lb5 = new JLabel("Disciplinas/Turma:");
        p2_1.add(lb5);

        JPanel p2_2 = new JPanel();
        listaDisciplinas = new JList();
        listaDisciplinas.setFixedCellWidth(200);
        listaDisciplinas.setEnabled(false);
        JScrollPane scrollPane = new JScrollPane(listaDisciplinas);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        p2_2.add(scrollPane);
        
        p2.add(p2_1);
        p2.add(p2_2);
        
        JPanel p3 = new JPanel();
        mostrarDisciplinas = new JButton("Mostrar");
        cancelar = new JButton("Cancelar");
        p3.add(mostrarDisciplinas);
        p3.add(cancelar);

        p.add(p1, BorderLayout.NORTH);
        p.add(p2, BorderLayout.CENTER);
        p.add(p3, BorderLayout.SOUTH);
        
        c.add(p);
        
        TrataEvento t = new TrataEvento();
        mostrarDisciplinas.addActionListener(t);
        cancelar.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == mostrarDisciplinas) {
                try {
                    String siglaProfessor = (String) (cmbProfessor.getSelectedItem());
                    ArrayList<String> dtlp = Horario.DisciplinasETurmaLecionaProfessor(siglaProfessor);
                    
                    if (dtlp.isEmpty()){
                        throw new DadoInvalidoException("Não há disciplinas/turmas lecionadas pelo professor " + siglaProfessor);
                    }
                    Horario.OrdenarAlunosTurmaAscendenteNome("1DA");
                    listaDisciplinas.setListData(dtlp.toArray());
                    
                }catch (DadoInvalidoException ex){
                    listaDisciplinas.setListData(new Object[0]);
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza um professor", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma percentagem de ocupação", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
