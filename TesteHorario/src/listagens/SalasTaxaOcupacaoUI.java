package listagens;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;
import java.util.ArrayList;

public class SalasTaxaOcupacaoUI extends JDialog {
    private JTextField txfPercOcup;
    private JButton mostrarSalas, cancelar;
    private JList listaSalas;
    
    public SalasTaxaOcupacaoUI (JFrame framePai){
        super(framePai, "Listar salas com um determinada taxa de ocupação", true);
        
        Container c = getContentPane();
        
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("Percentagem de ocupação:");
        txfPercOcup = new JTextField(5);
        txfPercOcup.setPreferredSize(new Dimension(79, 25));
        p1.add(lb1);
        p1.add(txfPercOcup);

        JPanel p2 = new JPanel();

        JPanel p2_1 = new JPanel();
        JLabel lb5 = new JLabel("Salas:");
        p2_1.add(lb5);

        JPanel p2_2 = new JPanel();
        listaSalas = new JList();
        listaSalas.setFixedCellWidth(200);
        listaSalas.setEnabled(false);
        JScrollPane scrollPane = new JScrollPane(listaSalas);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        p2_2.add(scrollPane);
        
        p2.add(p2_1);
        p2.add(p2_2);
        
        JPanel p3 = new JPanel();
        mostrarSalas = new JButton("Mostrar");
        cancelar = new JButton("Cancelar");
        p3.add(mostrarSalas);
        p3.add(cancelar);

        p.add(p1, BorderLayout.NORTH);
        p.add(p2, BorderLayout.CENTER);
        p.add(p3, BorderLayout.SOUTH);
        
        c.add(p);
        
        TrataEvento t = new TrataEvento();
        mostrarSalas.addActionListener(t);
        cancelar.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == mostrarSalas) {
                try {
                    ArrayList<Sala> salasOcupadas = Horario.OcupacaoSemanalSala(Integer.parseInt(txfPercOcup.getText()));
                    
                    if (salasOcupadas.isEmpty()){
                        throw new DadoInvalidoException("Não há salas com ocupação superior a " + txfPercOcup.getText() + "%");
                    }
                    
                    ArrayList<String> salasEtaxaOcup = new ArrayList();
                    for (Sala s : salasOcupadas){
                        salasEtaxaOcup.add(s.getCodigo() + " - " + ((float) Horario.CargaHorariaSemanalSala(s.getCodigo()) / 50 * 100) + "%");
                    }
                    
                    listaSalas.setListData(salasEtaxaOcup.toArray());
                    
                }catch (DadoInvalidoException ex){
                    listaSalas.setListData(new Object[0]);
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma percentagem de ocupação", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma percentagem de ocupação", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
