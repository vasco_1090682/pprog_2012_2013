package listagens;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;
import java.util.ArrayList;

public class ProfessoresPorTurmaUI extends JDialog {
    private JComboBox cmbTurma;
    private JButton mostrarProfessores, cancelar;
    private JList listaProfessores;
    
    public ProfessoresPorTurmaUI (JFrame framePai){
        super(framePai, "Listar os professores que dão aulas a uma turma", true);
        
        Container c = getContentPane();
        
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        JPanel p1 = new JPanel(new GridLayout(2, 1));
        
        JPanel p1_1 = new JPanel();
        JLabel lb1 = new JLabel("Turma:");
        cmbTurma = new JComboBox(Horario.DesignacoesTodasTurmas().toArray());
        cmbTurma.setPreferredSize(new Dimension(79, 25));
        cmbTurma.setSelectedIndex(-1);
        cmbTurma.setMaximumRowCount(10);
        p1_1.add(lb1);
        p1_1.add(cmbTurma);
        
        JPanel p1_2 = new JPanel();
        JLabel lb2 = new JLabel("Professores/Disciplina/Tipo Aula:");
        p1_2.add(lb2);

        p1.add(p1_1);
        p1.add(p1_2);
        

        JPanel p2 = new JPanel();
        listaProfessores = new JList();
        listaProfessores.setFixedCellWidth(200);
        listaProfessores.setEnabled(false);
        JScrollPane scrollPane = new JScrollPane(listaProfessores);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        p2.add(scrollPane);
        
        JPanel p3 = new JPanel();
        mostrarProfessores = new JButton("Mostrar");
        cancelar = new JButton("Cancelar");
        p3.add(mostrarProfessores);
        p3.add(cancelar);

        p.add(p1, BorderLayout.NORTH);
        p.add(p2, BorderLayout.CENTER);
        p.add(p3, BorderLayout.SOUTH);
        
        c.add(p);
        
        TrataEvento t = new TrataEvento();
        mostrarProfessores.addActionListener(t);
        cancelar.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == mostrarProfessores) {
                try {
                    String designacaoTurma = (String) (cmbTurma.getSelectedItem());
                    ArrayList<String> pt = Horario.ProfessoresTurma(designacaoTurma);
                    
                    if (pt.isEmpty()){
                        throw new DadoInvalidoException("Não há professores a leccionar aulas à tumra " + designacaoTurma);
                    }
                    
                    listaProfessores.setListData(pt.toArray());
                    
                }catch (DadoInvalidoException ex){
                    listaProfessores.setListData(new Object[0]);
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
