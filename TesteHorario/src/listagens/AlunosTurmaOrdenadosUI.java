package listagens;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;
import java.util.ArrayList;

public class AlunosTurmaOrdenadosUI extends JDialog{
    private JComboBox cmbTurma;
    private JButton crescente, decrescente, ascendente, descendente, cancelar;
    private JList listaAlunos;
    
    public AlunosTurmaOrdenadosUI (JFrame framePai){
        super(framePai, "Listar alunos de uma turma ordenadamente", true);
        
        Container c = getContentPane();
        
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("Turma:");
        cmbTurma = new JComboBox(Horario.DesignacoesTodasTurmas().toArray());
        cmbTurma.setPreferredSize(new Dimension(79, 25));
        cmbTurma.setSelectedIndex(-1);
        cmbTurma.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbTurma);


        JPanel p2 = new JPanel();

        JPanel p2_1 = new JPanel();
        JLabel lb5 = new JLabel("Alunos:");
        p2_1.add(lb5);

        JPanel p2_2 = new JPanel();
        listaAlunos = new JList();
        listaAlunos.setFixedCellWidth(700);
        listaAlunos.setEnabled(false);
        JScrollPane scrollPane = new JScrollPane(listaAlunos);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        p2_2.add(scrollPane);
        
        p2.add(p2_1);
        p2.add(p2_2);
        
        JPanel p3 = new JPanel();
        crescente = new JButton("Crescente");
        decrescente = new JButton("Decrescente");
        ascendente = new JButton("Ascendente");
        descendente = new JButton("Descendente");
        cancelar = new JButton("Cancelar");
        p3.add(crescente);
        p3.add(decrescente);
        p3.add(ascendente);
        p3.add(descendente);
        p3.add(cancelar);

        p.add(p1, BorderLayout.NORTH);
        p.add(p2, BorderLayout.CENTER);
        p.add(p3, BorderLayout.SOUTH);
        
        c.add(p);
        
        TrataEvento t = new TrataEvento();
        crescente.addActionListener(t);
        decrescente.addActionListener(t);
        ascendente.addActionListener(t);
        descendente.addActionListener(t);
        cancelar.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == crescente) {
                try {
                    String designacaoTurma = (String) (cmbTurma.getSelectedItem());
                    if (designacaoTurma != null || !designacaoTurma.isEmpty()){
                        Horario.OrdenarAlunosTurmaCrescenteNumero(designacaoTurma);
                        Turma t = Horario.RetornarTurma(designacaoTurma);
                        String str = "%d - %s - %s - %s - %d";
                        ArrayList<String> alunos = new ArrayList();
                        
                        for (Aluno a : t.getAlunos()){
                            String s = "";
                            s = String.format(str, a.getNumero(), a.getNome(), a.getEmail(), a.getDatanascimento().toAnoMesDiaString(), a.getTelefone());
                            alunos.add(s);
                        }
                        
                        listaAlunos.setListData(alunos.toArray());
                    }
                }catch (DadoInvalidoException ex){
                    listaAlunos.setListData(new Object[0]);
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else  if (e.getSource() == decrescente) {
                try {
                    String designacaoTurma = (String) (cmbTurma.getSelectedItem());
                    if (designacaoTurma != null || !designacaoTurma.isEmpty()){
                        Horario.OrdenarAlunosTurmaDecrescenteNumero(designacaoTurma);
                        Turma t = Horario.RetornarTurma(designacaoTurma);
                        String str = "%d - %s - %s - %s - %d";
                        ArrayList<String> alunos = new ArrayList();
                        
                        for (Aluno a : t.getAlunos()){
                            String s = "";
                            s = String.format(str, a.getNumero(), a.getNome(), a.getEmail(), a.getDatanascimento().toAnoMesDiaString(), a.getTelefone());
                            alunos.add(s);
                        }
                        
                        listaAlunos.setListData(alunos.toArray());
                    }
                }catch (DadoInvalidoException ex){
                    listaAlunos.setListData(new Object[0]);
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == ascendente) {
                try {
                    String designacaoTurma = (String) (cmbTurma.getSelectedItem());
                    if (designacaoTurma != null || !designacaoTurma.isEmpty()){
                        Horario.OrdenarAlunosTurmaAscendenteNome(designacaoTurma);
                        Turma t = Horario.RetornarTurma(designacaoTurma);
                        String str = "%s - %d - %s - %s - %d";
                        ArrayList<String> alunos = new ArrayList();
                        
                        for (Aluno a : t.getAlunos()){
                            String s = "";
                            s = String.format(str, a.getNome(), a.getNumero(), a.getEmail(), a.getDatanascimento().toAnoMesDiaString(), a.getTelefone());
                            alunos.add(s);
                        }
                        
                        listaAlunos.setListData(alunos.toArray());
                    }
                }catch (DadoInvalidoException ex){
                    listaAlunos.setListData(new Object[0]);
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == descendente) {
                try {
                    String designacaoTurma = (String) (cmbTurma.getSelectedItem());
                    if (designacaoTurma != null || !designacaoTurma.isEmpty()){
                        Horario.OrdenarAlunosTurmaDescendenteNome(designacaoTurma);
                        Turma t = Horario.RetornarTurma(designacaoTurma);
                        String str = "%s - %d - %s - %s - %d";
                        ArrayList<String> alunos = new ArrayList();
                        
                        for (Aluno a : t.getAlunos()){
                            String s = "";
                            s = String.format(str, a.getNome(), a.getNumero(), a.getEmail(), a.getDatanascimento().toAnoMesDiaString(), a.getTelefone());
                            alunos.add(s);
                        }
                        
                        listaAlunos.setListData(alunos.toArray());
                    }
                }catch (DadoInvalidoException ex){
                    listaAlunos.setListData(new Object[0]);
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Introduza uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
