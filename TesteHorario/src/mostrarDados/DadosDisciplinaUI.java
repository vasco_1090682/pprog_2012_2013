package mostrarDados;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class DadosDisciplinaUI extends JDialog {
    private JTextField txfDesignacao, txfHorasTeoricas, txfHorasPraticas;
    private JComboBox cmbDisciplina;
    private JButton dados, cancelar;
    private JList listaProfessores;
    
    public DadosDisciplinaUI (JFrame framePai){
        super(framePai, "Dados - Disciplina", true);
        
        Container c = getContentPane();
        
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        JPanel p1 = new JPanel(new GridLayout(4,1));
        
        JPanel p1_1 = new JPanel();
        JLabel lb1 = new JLabel("Disciplina:");
        cmbDisciplina = new JComboBox(Horario.SiglasTodasDisciplinas().toArray());
        cmbDisciplina.setPreferredSize(new Dimension(79, 25));
        cmbDisciplina.setSelectedIndex(-1);
        cmbDisciplina.setMaximumRowCount(10);
        p1_1.add(lb1);
        p1_1.add(cmbDisciplina);
        
        JPanel p1_2 = new JPanel();
        JLabel lb2 = new JLabel("     Designação:");
        txfDesignacao = new JTextField(25);
        txfDesignacao.setEnabled(false);
        p1_2.add(lb2);
        p1_2.add(txfDesignacao);
        
        JPanel p1_3 = new JPanel();
        JLabel lb3 = new JLabel("Horas teóricas:");
        txfHorasTeoricas = new JTextField(25);
        txfHorasTeoricas.setEnabled(false);
        p1_3.add(lb3);
        p1_3.add(txfHorasTeoricas);
        
        JPanel p1_4 = new JPanel();
        JLabel lb4 = new JLabel("Horas práticas:");
        txfHorasPraticas = new JTextField(25);
        txfHorasPraticas.setEnabled(false);
        p1_4.add(lb4);
        p1_4.add(txfHorasPraticas);
        
        p1.add(p1_1);
        p1.add(p1_2);
        p1.add(p1_3);
        p1.add(p1_4);
        
        JPanel p2 = new JPanel();

        JPanel p2_1 = new JPanel();
        JLabel lb5 = new JLabel("Professores:");
        p2_1.add(lb5);

        JPanel p2_2 = new JPanel();
        listaProfessores = new JList();
        listaProfessores.setFixedCellWidth(275);
        listaProfessores.setEnabled(true);
        JScrollPane scrollPane = new JScrollPane(listaProfessores);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        p2_2.add(scrollPane);
        
        listaProfessores.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if (listaProfessores.locationToIndex(e.getPoint()) != -1) {
                        String siglaProfessor = (String) listaProfessores.getSelectedValue();
                        System.out.println(siglaProfessor);
                        DadosProfessorUI d = new DadosProfessorUI(DadosDisciplinaUI.this, siglaProfessor);
                    }
                }
            }
        });
        
        
        p2.add(p2_1);
        p2.add(p2_2);
        
        JPanel p3 = new JPanel();
        dados = new JButton("Dados");
        cancelar = new JButton("Cancelar");
        p3.add(dados);
        p3.add(cancelar);

        p.add(p1, BorderLayout.NORTH);
        p.add(p2, BorderLayout.CENTER);
        p.add(p3, BorderLayout.SOUTH);
        
        c.add(p);
        
        TrataEvento t = new TrataEvento();
        dados.addActionListener(t);
        cancelar.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }
    
    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == dados) {
                try {
                    String siglaDisciplina = (String) (cmbDisciplina.getSelectedItem());
                    if (siglaDisciplina != null || !siglaDisciplina.isEmpty()){
                        Disciplina d = Horario.RetornarDisciplina(siglaDisciplina);
                        txfDesignacao.setText(d.getDesignacao());
                        txfHorasTeoricas.setText("" + d.getHorasTeoricas());
                        txfHorasPraticas.setText("" + d.getHorasPraticas());
                        listaProfessores.setListData(Horario.SiglasProfesoresDeUmaDisciplina(siglaDisciplina).toArray());
                    }
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma disciplina", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma disciplina", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
