package mostrarDados;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class DadosTurmaUI extends JDialog {

    private JComboBox cmbTurma;
    private JButton dados, cancelar;
    private JList listaAlunos;

    public DadosTurmaUI(JFrame framePai) {
        super(framePai, "Dados - Turma", true);

        Container c = getContentPane();

        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("Turma:");
        cmbTurma = new JComboBox(Horario.DesignacoesTodasTurmas().toArray());
        cmbTurma.setPreferredSize(new Dimension(79, 25));
        cmbTurma.setSelectedIndex(-1);
        cmbTurma.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbTurma);


        JPanel p2 = new JPanel();

        JPanel p2_1 = new JPanel();
        JLabel lb5 = new JLabel("Alunos:");
        p2_1.add(lb5);

        JPanel p2_2 = new JPanel();
        listaAlunos = new JList();
        listaAlunos.setFixedCellWidth(275);
        listaAlunos.setEnabled(true);
        JScrollPane scrollPane = new JScrollPane(listaAlunos);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        p2_2.add(scrollPane);


        listaAlunos.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if (listaAlunos.locationToIndex(e.getPoint()) != -1) {
                        String numero = (String) listaAlunos.getSelectedValue();
                        DadosAlunoUI d = new DadosAlunoUI(DadosTurmaUI.this, numero);
                    }
                }
            }
        });


        p2.add(p2_1);
        p2.add(p2_2);

        JPanel p3 = new JPanel();
        dados = new JButton("Dados");
        cancelar = new JButton("Cancelar");
        p3.add(dados);
        p3.add(cancelar);

        p.add(p1, BorderLayout.NORTH);
        p.add(p2, BorderLayout.CENTER);
        p.add(p3, BorderLayout.SOUTH);

        c.add(p);

        TrataEvento t = new TrataEvento();
        dados.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == dados) {
                try {
                    String designacaoTurma = (String) (cmbTurma.getSelectedItem());
                    if (designacaoTurma != null || !designacaoTurma.isEmpty()) {
                        listaAlunos.setListData(Horario.NumerosAlunosDeUmTurma(designacaoTurma).toArray());
                    }
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma turma", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
