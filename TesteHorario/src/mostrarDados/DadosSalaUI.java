package mostrarDados;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class DadosSalaUI extends JDialog{
    private JTextField txfTipo, txfCapacidade;
    private JComboBox cmbSala;
    private JButton dados, cancelar;
    
    public DadosSalaUI(JFrame framePai){
        super(framePai, "Dados - Sala", true);
        
        Container c = getContentPane();
        
        JPanel p = new JPanel(new GridLayout(5, 1));
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("Sala:");
        cmbSala = new JComboBox(Horario.CodigosTodasSalas().toArray());
        cmbSala.setPreferredSize(new Dimension(79, 25));
        cmbSala.setSelectedIndex(-1);
        cmbSala.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbSala);
        
        JPanel p2 = new JPanel();
        JLabel lb2 = new JLabel("                                     Tipo:");
        txfTipo = new JTextField(25);
        txfTipo.setEnabled(false);
        p2.add(lb2);
        p2.add(txfTipo);
        
        JPanel p3 = new JPanel();
        JLabel lb3 = new JLabel("                      Capacidade:");
        txfCapacidade = new JTextField(25);
        txfCapacidade.setEnabled(false);
        p3.add(lb3);
        p3.add(txfCapacidade);
        
        JPanel p4 = new JPanel();
        dados = new JButton("Dados");
        cancelar = new JButton("Cancelar");
        p4.add(dados);
        p4.add(cancelar);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        
        c.add(p);
        
        TrataEvento t = new TrataEvento();
        dados.addActionListener(t);
        cancelar.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }
    
    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == dados) {
                try {
                    String codigoSala = (String) (cmbSala.getSelectedItem());
                    if (codigoSala != null || !codigoSala.isEmpty()) {
                        Sala s = Horario.RetornarSala(codigoSala);
                        txfTipo.setText(s.getTipo().toString());
                        txfCapacidade.setText("" + s.getCapacidade());
                    }
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma sala", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione uma sala", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
