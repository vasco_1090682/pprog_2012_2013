package mostrarDados;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class DadosProfessorUI extends JDialog {

    private JTextField txfNome, txfEmail, txfDataNascimento;
    private JComboBox cmbProfessor;
    private JButton dados, cancelar;

    public DadosProfessorUI(JFrame framePai) {
        super(framePai, "Dados - Professor", true);

        Container c = getContentPane();

        JPanel p = new JPanel(new GridLayout(5, 1));
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("Professor:");
        cmbProfessor = new JComboBox(Horario.SiglasTodosProfessores().toArray());
        cmbProfessor.setPreferredSize(new Dimension(79, 25));
        cmbProfessor.setSelectedIndex(-1);
        cmbProfessor.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbProfessor);

        JPanel p2 = new JPanel();
        JLabel lb2 = new JLabel("                      Nome:");
        txfNome = new JTextField(25);
        txfNome.setEnabled(false);
        p2.add(lb2);
        p2.add(txfNome);

        JPanel p3 = new JPanel();
        JLabel lb3 = new JLabel("                      Email:");
        txfEmail = new JTextField(25);
        txfEmail.setEnabled(false);
        p3.add(lb3);
        p3.add(txfEmail);

        JPanel p4 = new JPanel();
        JLabel lb4 = new JLabel("Data contratação:");
        txfDataNascimento = new JTextField(25);
        txfDataNascimento.setEnabled(false);
        p4.add(lb4);
        p4.add(txfDataNascimento);

        JPanel p5 = new JPanel();
        dados = new JButton("Dados");
        cancelar = new JButton("Cancelar");
        p5.add(dados);
        p5.add(cancelar);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);

        c.add(p);

        TrataEvento t = new TrataEvento();
        dados.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    public DadosProfessorUI(JDialog framePai, String sigla) {
        super(framePai, "Dados - Professor", true);

        Container c = getContentPane();

        JPanel p = new JPanel(new GridLayout(5, 1));
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("Professor:");
        cmbProfessor = new JComboBox(Horario.SiglasTodosProfessores().toArray());
        cmbProfessor.setPreferredSize(new Dimension(79, 25));
        cmbProfessor.setSelectedIndex(-1);
        cmbProfessor.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbProfessor);

        JPanel p2 = new JPanel();
        JLabel lb2 = new JLabel("                      Nome:");
        txfNome = new JTextField(25);
        txfNome.setEnabled(false);
        p2.add(lb2);
        p2.add(txfNome);

        JPanel p3 = new JPanel();
        JLabel lb3 = new JLabel("                      Email:");
        txfEmail = new JTextField(25);
        txfEmail.setEnabled(false);
        p3.add(lb3);
        p3.add(txfEmail);

        JPanel p4 = new JPanel();
        JLabel lb4 = new JLabel("Data contratação:");
        txfDataNascimento = new JTextField(25);
        txfDataNascimento.setEnabled(false);
        p4.add(lb4);
        p4.add(txfDataNascimento);

        JPanel p5 = new JPanel();
        dados = new JButton("Dados");
        cancelar = new JButton("Cancelar");
        p5.add(dados);
        p5.add(cancelar);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);

        c.add(p);
        
        //valores predefinidos
        cmbProfessor.setSelectedItem(sigla);
        Professor pf = Horario.RetornarProfessor(sigla);
        txfNome.setText(pf.getNome());
        txfEmail.setText(pf.getEmail());
        txfDataNascimento.setText("" + pf.getDatacontrato().toAnoMesDiaString());

        TrataEvento t = new TrataEvento();
        dados.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == dados) {
                try {
                    String siglaProfessor = (String) (cmbProfessor.getSelectedItem());
                    if (siglaProfessor != null || !siglaProfessor.isEmpty()) {
                        Professor p = Horario.RetornarProfessor(siglaProfessor);
                        txfNome.setText(p.getNome());
                        txfEmail.setText(p.getEmail());
                        txfDataNascimento.setText("" + p.getDatacontrato().toAnoMesDiaString());
                    }
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um professor", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um professor", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
