package mostrarDados;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import horario.*;

public class DadosAlunoUI extends JDialog {

    private JTextField txfNome, txfEmail, txfDataNascimento, txfTefelone;
    private JComboBox cmbAluno;
    private JButton dados, cancelar;

    public DadosAlunoUI(JFrame framePai) {
        super(framePai, "Dados - Aluno", true);

        Container c = getContentPane();

        JPanel p = new JPanel(new GridLayout(6, 1));
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("Aluno:");
        cmbAluno = new JComboBox(Horario.NumerosTodosAlunos().toArray());
        cmbAluno.setPreferredSize(new Dimension(79, 25));
        cmbAluno.setSelectedIndex(-1);
        cmbAluno.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbAluno);

        JPanel p2 = new JPanel();
        JLabel lb2 = new JLabel("                     Nome:");
        txfNome = new JTextField(25);
        txfNome.setEnabled(false);
        p2.add(lb2);
        p2.add(txfNome);

        JPanel p3 = new JPanel();
        JLabel lb3 = new JLabel("                     Email:");
        txfEmail = new JTextField(25);
        txfEmail.setEnabled(false);
        p3.add(lb3);
        p3.add(txfEmail);

        JPanel p4 = new JPanel();
        JLabel lb4 = new JLabel("Data nascimento:");
        txfDataNascimento = new JTextField(25);
        txfDataNascimento.setEnabled(false);
        p4.add(lb4);
        p4.add(txfDataNascimento);

        JPanel p5 = new JPanel();
        JLabel lb5 = new JLabel("               Telefone:");
        txfTefelone = new JTextField(25);
        txfTefelone.setEnabled(false);
        p5.add(lb5);
        p5.add(txfTefelone);

        JPanel p6 = new JPanel();
        dados = new JButton("Dados");
        cancelar = new JButton("Cancelar");
        p6.add(dados);
        p6.add(cancelar);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        p.add(p6);

        c.add(p);

        TrataEvento t = new TrataEvento();
        dados.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    public DadosAlunoUI(JDialog framePai, String numero) {
        super(framePai, "Dados - Aluno", true);

        Container c = getContentPane();

        JPanel p = new JPanel(new GridLayout(6, 1));
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel();
        JLabel lb1 = new JLabel("Aluno:");
        cmbAluno = new JComboBox(Horario.NumerosTodosAlunos().toArray());
        cmbAluno.setPreferredSize(new Dimension(79, 25));
        cmbAluno.setSelectedIndex(-1);
        cmbAluno.setMaximumRowCount(10);
        p1.add(lb1);
        p1.add(cmbAluno);

        JPanel p2 = new JPanel();
        JLabel lb2 = new JLabel("                     Nome:");
        txfNome = new JTextField(25);
        txfNome.setEnabled(false);
        p2.add(lb2);
        p2.add(txfNome);

        JPanel p3 = new JPanel();
        JLabel lb3 = new JLabel("                     Email:");
        txfEmail = new JTextField(25);
        txfEmail.setEnabled(false);
        p3.add(lb3);
        p3.add(txfEmail);

        JPanel p4 = new JPanel();
        JLabel lb4 = new JLabel("Data nascimento:");
        txfDataNascimento = new JTextField(25);
        txfDataNascimento.setEnabled(false);
        p4.add(lb4);
        p4.add(txfDataNascimento);

        JPanel p5 = new JPanel();
        JLabel lb5 = new JLabel("               Telefone:");
        txfTefelone = new JTextField(25);
        txfTefelone.setEnabled(false);
        p5.add(lb5);
        p5.add(txfTefelone);

        JPanel p6 = new JPanel();
        dados = new JButton("Dados");
        cancelar = new JButton("Cancelar");
        p6.add(dados);
        p6.add(cancelar);

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        p.add(p6);

        c.add(p);


        //valores predefinidos
        cmbAluno.setSelectedItem(numero);
        int num = Integer.parseInt(numero);
        Aluno al = Horario.RetornarAluno(num);
        txfNome.setText(al.getNome());
        txfEmail.setText(al.getEmail());
        txfDataNascimento.setText("" + al.getDatanascimento().toAnoMesDiaString());
        txfTefelone.setText("" + al.getTelefone());

        TrataEvento t = new TrataEvento();
        dados.addActionListener(t);
        cancelar.addActionListener(t);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == dados) {
                try {
                    int numeroAluno = Integer.parseInt((String) (cmbAluno.getSelectedItem()));
                    Aluno a = Horario.RetornarAluno(numeroAluno);
                    txfNome.setText(a.getNome());
                    txfEmail.setText(a.getEmail());
                    txfDataNascimento.setText("" + a.getDatanascimento().toAnoMesDiaString());
                    txfTefelone.setText("" + a.getTelefone());

                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um aluno", "Aviso", JOptionPane.WARNING_MESSAGE);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Selecione um aluno", "Aviso", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                dispose();
            }
        }
    }
}
