package horario;

import utilitarios.Data;

public class Professor extends Pessoa {
    //Variáveis de classe

    //Variáveis de instância
    private String sigla;
    private Data datacontrato;

    //Construtores
    public Professor(String nome, String email, String sigla, Data datacontrato) {
        super(nome, email);
        setSigla(sigla);
        setDatacontrato(datacontrato);
    }

    public Professor() {
        this("", "", "", new Data());
    }

    public Professor(Professor p) {
        this(p.getNome(), p.getEmail(), p.sigla, p.datacontrato);
    }

    //Métodos de classe
    //Métodos de consulta

    //Métodos modificadores

    //Métodos de instância
    //Métodos de consulta
    public String getSigla() {
        return sigla;
    }

    public Data getDatacontrato() {
        return datacontrato;
    }

    //Métodos modificadores
    public void setSigla(String sigla) {
        this.sigla = sigla == null || sigla.isEmpty() ? "sem sigla" : sigla;
    }

    public void setDatacontrato(Data datacontrato) {
        this.datacontrato = datacontrato;
    }

    //Métodos complementares
    public String toString() {
        return super.toString() + " | Sigla: " + sigla + " | Data contratação: " + datacontrato.toAnoMesDiaString();
    }

    public Professor clone() {
        return new Professor(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Professor p = (Professor) obj;
        return getNome().equals(p.getNome()) && getEmail().equals(p.getEmail()) && sigla == p.sigla && datacontrato == p.datacontrato;
    }
}
