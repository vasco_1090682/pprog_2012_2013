package horario;

import java.io.Serializable;
import java.util.ArrayList;

public class Disciplina implements Serializable{
    //Variáveis de classe

    //Variáveis de instância
    private String sigla;
    private String designacao;
    private int horasTeoricas;
    private int horasPraticas;
    private ArrayList<Professor> professores;
    
    //Construtores
    public Disciplina(String sigla, String designacao, int horasTeoricas, int horasPraticas, ArrayList<Professor> professores) {
        setSigla(sigla);
        setDesignacao(designacao);
        setHorasTeoricas(horasTeoricas);
        setHorasPraticas(horasPraticas);
        setProfessores(professores);
    }

    public Disciplina() {
        this("", "", 0, 0, new ArrayList<Professor>());
    }

    public Disciplina(Disciplina d) {
        this(d.sigla, d.designacao, d.horasTeoricas, d.horasPraticas, d.professores);
    }
    
    //Métodos de classe
    //Métodos de consulta

    //Métodos modificadores

    //Métodos de instância
    //Métodos de consulta
    public String getSigla() {
        return sigla;
    }

    public String getDesignacao() {
        return designacao;
    }

    public int getHorasTeoricas() {
        return horasTeoricas;
    }

    public int getHorasPraticas() {
        return horasPraticas;
    }

    public ArrayList<Professor> getProfessores() {
        return professores;
    }
    
    //Métodos modificadores
    public void setSigla(String sigla) {
        this.sigla = sigla == null || sigla.isEmpty() ? "sem sigla" : sigla;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao == null || designacao.isEmpty() ? "sem designação" : designacao;
    }

    public void setHorasTeoricas(int horasTeoricas) {
        this.horasTeoricas = horasTeoricas > 0 && horasTeoricas < 50 ? horasTeoricas : 0;
    }

    public void setHorasPraticas(int horasPraticas) {
        this.horasPraticas = horasPraticas > 0 && horasPraticas < 50 ? horasPraticas : 0;
    }

    public void setProfessores(ArrayList<Professor> professores) {
        this.professores = professores;
    }

    //Métodos complementares
    public String toString() {
        String disciplina = "Sigla: " + sigla + " | Designação: " + designacao + " | N.º horas teóricas semanais: " + horasTeoricas + " | N.º horas teóricas semanais: " + horasPraticas + "\nProfessores:";
        for (Professor p : professores) {
            disciplina += "\n" + p.toString();
        }
        return disciplina;
    }
    
    public Disciplina clone() {
        return new Disciplina(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Disciplina d = (Disciplina) obj;
        return sigla.equals(d.sigla) && designacao.equals(d.designacao) && horasTeoricas == d.horasTeoricas && horasPraticas == d.horasPraticas && professores == d.professores;
    }
}
