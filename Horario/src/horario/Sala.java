package horario;

import java.io.Serializable;

public class Sala implements Serializable {
    //Variáveis de classe

    //Variáveis enumeradas
    public enum TipoSala{
        laboratorio, anfiteatro 
    }
    
    //Variáveis de instância
    private String codigo;
    private TipoSala tipo;
    private int capacidade;
    
    //Construtores
    public Sala(String codigo, TipoSala tipo, int capacidade) {
        setCodigo(codigo);
        this.tipo = tipo;
        setCapacidade(capacidade);
    }

    public Sala() {
        this("", null, 0);
    }

    public Sala(Sala s) {
        this(s.codigo, s.tipo, s.capacidade);
    }
    
    //Métodos de classe
    //Métodos de consulta

    //Métodos modificadores

    //Métodos de instância
    //Métodos de consulta
    public String getCodigo() {
        return codigo;
    }

    public TipoSala getTipo() {
        return tipo;
    }

    public int getCapacidade() {
        return capacidade;
    }

    //Métodos modificadores
    public void setCodigo(String codigo) {
        this.codigo = codigo == null || codigo.isEmpty() ? "sem código" : codigo;
    }

    public void setTipo(TipoSala tipo) {
        this.tipo = tipo;
    }
    
    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade > 0 ? capacidade : 0;
    }
    
    //Métodos complementares
    public String toString() {
        return "Sala: " + codigo + " | Tipo da Sala: " + tipo + " | Capacidade: " + capacidade;
    }
    
    public Sala clone() {
        return new Sala(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Sala s = (Sala) obj;
        return codigo.equals(s.codigo) && tipo == s.tipo && capacidade == s.capacidade;
    }
}
