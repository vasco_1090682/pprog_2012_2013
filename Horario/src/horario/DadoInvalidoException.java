package horario;

public class DadoInvalidoException extends Exception {

    public DadoInvalidoException(String msg) {
        super(msg);
    }
}
