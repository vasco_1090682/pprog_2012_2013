package horario;

import java.io.Serializable;
import java.util.ArrayList;

public class Turma implements Serializable{
    //Variáveis de classe

    //Variáveis de instância
    private String designacao;
    private ArrayList<Aluno> alunos;
    
    //Construtores
    public Turma(String designacao, ArrayList<Aluno> alunos) {
        setDesignacao(designacao);
        setAlunos(alunos);
    }

    public Turma() {
        this("", new ArrayList<Aluno>());
    }

    public Turma(Turma t) {
        this(t.designacao, t.alunos);
    }

    //Métodos de classe
    //Métodos de consulta

    //Métodos modificadores

    //Métodos de instância
    //Métodos de consulta
    public String getDesignacao() {
        return designacao;
    }

    public ArrayList<Aluno> getAlunos() {
        return alunos;
    }

    //Métodos modificadores
    public void setDesignacao(String designacao) {
        this.designacao = designacao == null || designacao.isEmpty() ? "sem designção" : designacao;
    }

    public void setAlunos(ArrayList<Aluno> alunos) {
        this.alunos = alunos;
    }

    //Métodos complementares
    public String toString() {
        String turma = "Turma: " + designacao + "\nAlunos:";
        for (Aluno a : alunos) {
            turma += "\n" + a.toString();
        }
        return turma;
    }
    
    public Turma clone() {
        return new Turma(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Turma t = (Turma) obj;
        return designacao.equals(t.designacao) && alunos == t.alunos;
    }
}
