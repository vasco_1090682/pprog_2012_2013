package horario;

import java.io.Serializable;

public abstract class Pessoa implements Serializable {
    //Variáveis de classe

    //Variáveis de instância
    private String nome;
    private String email;

    //Construtores
    public Pessoa(String nome, String email) {
        setNome(nome);
        setEmail(email);
    }

    public Pessoa() {
        this("", "");
    }
    
    public Pessoa(Pessoa p) {
        this(p.nome, p.email);
    }

    //Métodos de classe
    //Métodos de consulta

    //Métodos modificadores

    //Métodos de instância
    //Métodos de consulta
    public String getNome(){
        return nome;
    }
    
    public String getEmail(){
        return email;
    }

    //Métodos modificadores
    public void setNome(String nome) {
        this.nome = nome == null || nome.isEmpty() ? "sem nome" : nome;
    }
    
    public void setEmail(String email) {
        this.email = email == null || email.isEmpty() ? "sem email" : email;
    }

    //Métodos complementares
    public String toString(){
        return "Nome: " + nome + " | E-mail: " + email;
    }
    
    public abstract Pessoa clone();
    
    public abstract boolean equals(Object obj);
}
