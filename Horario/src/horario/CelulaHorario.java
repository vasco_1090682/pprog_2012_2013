package horario;

import java.io.Serializable;

public class CelulaHorario implements Serializable{
    //Variáveis de classe
    
    //Variáveis enumeradas
    public enum TipoAula{
        T, P;
    }

    //Variáveis de instância
    private Turma turma;
    private Disciplina disciplina;
    private TipoAula tipo;
    private int diaSemana;
    private int horaInicio;
    private int duracao;
    private Professor professor;
    private Sala sala;
    
    //Construtores
    public CelulaHorario(Turma turma, Disciplina siglaDisciplina, TipoAula tipo, int diaSemana, int horaInicio, int duracao, Professor siglaProfessor, Sala sala) {
        setTurma(turma);
        setDisciplina(siglaDisciplina);
        this.tipo = tipo;
        setDiaSemana(diaSemana);
        setHoraInicio(horaInicio);
        setDuracao(duracao);
        setProfessor(siglaProfessor);
        setSala(sala);
    }

    public CelulaHorario() {
        this(null, null, null, 0, 0, 0, null, null);
    }

    public CelulaHorario(CelulaHorario ch) {
        this(ch.turma, ch.disciplina, ch.tipo, ch.diaSemana, ch.horaInicio, ch.duracao, ch.professor, ch.sala);
    }
    
    //Métodos de classe
    //Métodos de consulta

    //Métodos modificadores

    //Métodos de instância
    //Métodos de consulta
    public Turma getTurma() {
        return turma;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public TipoAula getTipoAula() {
        return tipo;
    }

    public int getDiaSemana() {
        return diaSemana;
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    public int getDuracao() {
        return duracao;
    }

    public Professor getProfessor() {
        return professor;
    }

    public Sala getSala() {
        return sala;
    }

    //Métodos modificadores
    public void setTurma(Turma turma) {
        this.turma = turma;
    }

    public void setDisciplina(Disciplina siglaDisciplina) {
        this.disciplina = siglaDisciplina;
    }
    
    public void setTipoAula(TipoAula tipo) {
        this.tipo = tipo;
    }

    public void setDiaSemana(int diaSemana) {
        this.diaSemana = diaSemana;
    }

    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public void setProfessor(Professor siglaProfessor) {
        this.professor = siglaProfessor;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    //Métodos complementares
    public String toString() {
        return diaSemana + " | " + horaInicio + " | " + duracao + "\n" + turma.getDesignacao() + " | " + disciplina.getSigla() + " | " + tipo + "\n" + professor.getSigla() + "\n" + sala.getCodigo();
    }

    public CelulaHorario clone() {
        return new CelulaHorario(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        CelulaHorario ch = (CelulaHorario) obj;
        return turma.equals(ch.turma) && disciplina.equals(ch.disciplina) && tipo == ch.tipo && diaSemana == ch.diaSemana && horaInicio == ch.horaInicio && duracao == ch.duracao && professor.equals(ch.professor) && sala.equals(ch.sala);
    }

}
