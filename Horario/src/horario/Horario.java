package horario;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
import java.util.Comparator;
import utilitarios.Data;

public class Horario {

    private static ArrayList<Aluno> alunos = new ArrayList();
    private static ArrayList<Professor> professores = new ArrayList();
    private static ArrayList<Sala> salas = new ArrayList();
    private static ArrayList<Disciplina> disciplinas = new ArrayList();
    private static ArrayList<Turma> turmas = new ArrayList();
    ;
    private static ArrayList<CelulaHorario> celulashorarios = new ArrayList();
    private static String[] diasDaSemana = {"2", "3", "4", "5", "6"};
    private static String[] horasInicio = {"8", "9", "10", "11", "12", "13", "14", "15", "16", "17"};
    private static String[] duracoes = {"1", "2"};

    //Métodos de consulta
    public static ArrayList<Aluno> getAlunos() {
        return alunos;
    }

    public static ArrayList<Professor> getProfessores() {
        return professores;
    }

    public static ArrayList<Sala> getSalas() {
        return salas;
    }

    public static ArrayList<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public static ArrayList<Turma> getTurmas() {
        return turmas;
    }

    public static ArrayList<CelulaHorario> getCelulashorarios() {
        return celulashorarios;
    }

    public static String[] getDiasDaSemana() {
        return diasDaSemana;
    }

    public static String[] getHorasInicio() {
        return horasInicio;
    }

    public static String[] getDuracoes() {
        return duracoes;
    }

    //Métodos complementares
    public static void LerFicheiroAlunos() throws FileNotFoundException {
        File f = new File("alunos.txt");
        try {
            Scanner lerf = new Scanner(f);
            String linha;
            lerf.nextLine(); //avança a primeira linha do ficheiro (cabeçalho)

            while (lerf.hasNextLine()) {
                linha = lerf.nextLine();
                String[] campos = linha.split(";");

                String nome = campos[1].trim();
                String email = campos[4].trim();
                int numero = Integer.parseInt(campos[0].trim());

                String[] camposdata = campos[2].split("/");
                int ano = Integer.parseInt(camposdata[2].trim());
                int mes = Integer.parseInt(camposdata[1].trim());
                int dia = Integer.parseInt(camposdata[0].trim());
                Data d = new Data(ano, mes, dia);

                int telefone = Integer.parseInt(campos[3].trim());
                alunos.add(new Aluno(nome, email, numero, d, telefone));
            }
        } catch (FileNotFoundException fnf) {
            throw new FileNotFoundException("Ficheiros .txt em falta");
        }
    }

    public static void LerFicheiroProfessores() throws FileNotFoundException {
        File f = new File("professores.txt");
        try {
            Scanner lerf = new Scanner(f);
            String linha;

            lerf.nextLine(); //avança a primeira linha do ficheiro (cabeçalho)
            while (lerf.hasNextLine()) {
                linha = lerf.nextLine();
                String[] campos = linha.split(";");

                String nome = campos[1].trim();
                String email = campos[3].trim();
                String sigla = campos[0].trim();

                String[] camposdata = campos[2].split("/");
                int ano = Integer.parseInt(camposdata[2].trim());
                int mes = Integer.parseInt(camposdata[1].trim());
                int dia = Integer.parseInt(camposdata[0].trim());
                Data d = new Data(ano, mes, dia);

                professores.add(new Professor(nome, email, sigla, d));
            }
            lerf.close();

        } catch (FileNotFoundException fnf) {
            throw new FileNotFoundException("Ficheiros .txt em falta");
        }
    }

    public static void LerFicheiroSalas() throws FileNotFoundException {
        File f = new File("salas.txt");
        try {
            Scanner lerf = new Scanner(f);
            String linha;

            lerf.nextLine(); //avança a primeira linha do ficheiro (cabeçalho)
            while (lerf.hasNextLine()) {
                linha = lerf.nextLine();
                String[] campos = linha.split(";");
                salas.add(new Sala(campos[0].trim(), Sala.TipoSala.valueOf(campos[1].trim()), Integer.parseInt(campos[2].trim())));
            }
            lerf.close();
        } catch (FileNotFoundException fnf) {
            throw new FileNotFoundException("Ficheiros .txt em falta");
        }
    }

    public static void LerFicheiroTurmas() throws FileNotFoundException {
        File f = new File("turmas.txt");
        try {
            Scanner lerf = new Scanner(f);
            String linha;

            lerf.nextLine(); //avança a primeira linha do ficheiro (cabeçalho)
            while (lerf.hasNextLine()) {
                linha = lerf.nextLine();
                String[] campos = linha.split(";");

                ArrayList<Aluno> al = new ArrayList<Aluno>();

                for (int i = 1; i < campos.length; i++) {
                    int n = Integer.parseInt(campos[i].trim());
                    for (Aluno a : alunos) {
                        if (n == a.getNumero()) {
                            al.add(a);
                        }
                    }
                }
                turmas.add(new Turma(campos[0].trim(), al));
            }
            lerf.close();
        } catch (FileNotFoundException fnf) {
            throw new FileNotFoundException("Ficheiros .txt em falta");
        }
    }

    public static void LerFicheiroDisciplinas() throws FileNotFoundException {
        File f = new File("disciplinas.txt");
        try {
            Scanner lerf = new Scanner(f);
            String linha;

            lerf.nextLine(); //avança a primeira linha do ficheiro (cabeçalho)
            while (lerf.hasNextLine()) {
                linha = lerf.nextLine();
                String[] campos = linha.split(";");

                String sigla = campos[0].trim();
                String designacao = campos[1].trim();
                int horasT = Integer.parseInt(campos[2].trim());
                int horasP = Integer.parseInt(campos[3].trim());

                ArrayList<Professor> pf = new ArrayList<Professor>();

                for (int i = 4; i < campos.length; i++) {
                    for (Professor p : professores) {
                        if (campos[i].trim().equals(p.getSigla())) {
                            pf.add(p);
                        }
                    }
                }

                disciplinas.add(new Disciplina(sigla, designacao, horasT, horasP, pf));
            }
            lerf.close();
        } catch (FileNotFoundException fnf) {
            throw new FileNotFoundException("Ficheiros .txt em falta");
        }
    }

    public static void LerFicheiroCelulasHorarios() throws FileNotFoundException {
        File f = new File("horarios.txt");
        try {
            Scanner lerf = new Scanner(f);
            String linha;

            lerf.nextLine(); //avança a primeira linha do ficheiro (cabeçalho)
            while (lerf.hasNextLine()) {
                linha = lerf.nextLine();
                String[] campos = linha.split(";");

                Turma auxt = new Turma();
                for (Turma t : turmas) {
                    if (t.getDesignacao().equals(campos[0].trim())) {
                        auxt = t.clone();
                    }
                }

                Disciplina auxd = new Disciplina();
                for (Disciplina d : disciplinas) {
                    if (d.getSigla().equals(campos[1].trim())) {
                        auxd = d.clone();
                    }
                }

                int diaSemana = Integer.parseInt(campos[3].trim());
                int horaInicio = Integer.parseInt(campos[4].trim());
                int duracao = Integer.parseInt(campos[5].trim());

                Professor auxp = new Professor();
                for (Professor p : professores) {
                    if (p.getSigla().equals(campos[6].trim())) {
                        auxp = p.clone();
                    }
                }

                Sala auxs = new Sala();
                for (Sala s : salas) {
                    if (s.getCodigo().equals(campos[7].trim())) {
                        auxs = s.clone();
                    }
                }

                celulashorarios.add(new CelulaHorario(auxt, auxd, CelulaHorario.TipoAula.valueOf(campos[2].trim()), diaSemana, horaInicio, duracao, auxp, auxs));
            }
            lerf.close();
        } catch (FileNotFoundException fnf) {
            throw new FileNotFoundException("Ficheiro '" + f + "' não encontrado na pasta da aplicação");
        }
    }

    public static void ValidarDiaSemana(int diaSemana) throws DadoInvalidoException {
        if (diaSemana < 2 || diaSemana > 6) {
            throw new DadoInvalidoException("Dia da semana não deve ser menor que 2 ou maior que 6");
        }
    }

    public static void ValidarHoraInicio(int horaInicio) throws DadoInvalidoException {
        if (horaInicio < 8 || horaInicio > 17) {
            throw new DadoInvalidoException("Hora início não deve ser menor que 8 ou maior que 17");
        }
    }

    public static void ValidarDuracao(int duracao) throws DadoInvalidoException {
        if (duracao < 1 || duracao > 2) {
            throw new DadoInvalidoException("Duracção só pode ser de 1 ou 2 horas");
        }
    }

    public static void ValidarHoraInicioEDuracao(int horaInicio, int duracao) throws DadoInvalidoException {
        ValidarHoraInicio(horaInicio);
        ValidarDuracao(duracao);
        if (horaInicio == 17 && duracao == 2) {
            throw new DadoInvalidoException("Às 17:00 não é possível ter uma aula com duração de 2 horas");
        }
    }

    public static void ValidarAluno(int numeroAluno) throws DadoInvalidoException {
        int cont = 1;
        for (Aluno c : alunos) {
            if (c.getNumero() == numeroAluno) {
                break;
            } else if (cont == alunos.size()) {
                throw new DadoInvalidoException("O(a) aluno(a) " + numeroAluno + "não existe");
            }
            cont++;
        }
    }

    public static void ValidarProfessor(String siglaProfessor) throws DadoInvalidoException {
        if (siglaProfessor == null || siglaProfessor == "") {
            throw new DadoInvalidoException("Introduza um professor");
        }
        int cont = 1;
        for (Professor p : professores) {
            if (p.getSigla().equals(siglaProfessor)) {
                break;
            } else if (cont == professores.size()) {
                throw new DadoInvalidoException("O(a) professor(a) " + siglaProfessor + " não existe");
            }
            cont++;
        }
    }

    public static void ValidarTurma(String designacaoTurma) throws DadoInvalidoException {
        if (designacaoTurma == null || designacaoTurma == "") {
            throw new DadoInvalidoException("Introduza uma turma");
        }
        int cont = 1;
        for (Turma t : turmas) {
            if (t.getDesignacao().equals(designacaoTurma)) {
                break;
            } else if (cont == turmas.size()) {
                throw new DadoInvalidoException("A turma " + designacaoTurma + " não existe");
            }
            cont++;
        }
    }

    public static void ValidarDisciplina(String siglaDisciplina) throws DadoInvalidoException {
        if (siglaDisciplina == null || siglaDisciplina.equals("")) {
            throw new DadoInvalidoException("Introduza uma disciplina");
        }
        int cont = 1;
        for (Disciplina d : disciplinas) {
            if (d.getSigla().equals(siglaDisciplina)) {
                break;
            } else if (cont == disciplinas.size()) {
                throw new DadoInvalidoException("A disciplina " + siglaDisciplina + " não existe");
            }
            cont++;
        }
    }

    public static void ValidarSala(String codigoSala) throws DadoInvalidoException {
        if (codigoSala == null || codigoSala == "") {
            throw new DadoInvalidoException("Introduza uma sala");
        }
        int cont = 1;
        for (Sala s : salas) {
            if (s.getCodigo().equals(codigoSala)) {
                break;
            } else if (cont == salas.size()) {
                throw new DadoInvalidoException("A sala " + codigoSala + " não existe");
            }
            cont++;
        }
    }

    public static void ValidarCelulaHorario(String designacaoTurma, String siglaDisciplina, String tipoAula, int diaSemana, int horaInicio, int duracao, String siglaProfessor, String codigoSala) throws DadoInvalidoException {
        int cont = 1;
        for (CelulaHorario c : celulashorarios) {
            if (c.getTurma().getDesignacao().equals(designacaoTurma) && c.getDisciplina().getSigla().equals(siglaDisciplina) && c.getTipoAula() == CelulaHorario.TipoAula.valueOf(tipoAula) && c.getDiaSemana() == diaSemana && c.getHoraInicio() == horaInicio && c.getDuracao() == duracao && c.getProfessor().getSigla().equals(siglaProfessor) && c.getSala().getCodigo().equals(codigoSala)) {
                break;
            } else if (cont == celulashorarios.size()) {
                throw new DadoInvalidoException("Célula inexistente");
            }
            cont++;
        }
    }

    public static void ValidarTipoAulaComTipoSala(Sala sala, String tipoAula) throws DadoInvalidoException {
        if (sala.getTipo() == Sala.TipoSala.laboratorio && tipoAula.equals("T")) {
            throw new DadoInvalidoException("Na sala " + sala.getCodigo() + " só podem ser lecionadas aulas do tipo " + CelulaHorario.TipoAula.P);
        } else if (sala.getTipo() == Sala.TipoSala.anfiteatro && tipoAula.equals("P")) {
            throw new DadoInvalidoException("Na sala " + sala.getCodigo() + " só podem ser lccionadas aulas do tipo " + CelulaHorario.TipoAula.T);
        }
    }

    public static void EliminarCelulaTurma(String designacaoTurma, int diaSemana, int horaInicio) throws DadoInvalidoException {
        ValidarTurma(designacaoTurma);
        ValidarDiaSemana(diaSemana);
        ValidarHoraInicio(horaInicio);
        int cont = 1;
        for (CelulaHorario c : celulashorarios) {
            if (c.getTurma().getDesignacao().equals(designacaoTurma) && c.getDiaSemana() == diaSemana && c.getHoraInicio() == horaInicio) {
                celulashorarios.remove(c);
                break;
            } else if (cont == celulashorarios.size()) {
                throw new DadoInvalidoException("Célula da turma " + designacaoTurma + " " + Data.diaSemana2(diaSemana - 1) + "/" + horaInicio + ":00 inexistente");
            }
            cont++;
        }
    }

    public static void ImprimirHorario(ArrayList<CelulaHorario> horario) {
        String separador = "|";
        String linha1 = "+--------+";
        String linha2 = "--------------------------+";
        String linha3 = "                          +";
        String infoCelula = "%s%3s%8s%5s%5s%3s";

        System.out.println("+-----------------------------------------------------------------------------------------------------------------------------------------------+");
        String[] diasSemana = {"Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira"};
        System.out.format("|        | %18s       |%18s        |%18s        |%18s        |%18s        |\n", diasSemana[0], diasSemana[1], diasSemana[2], diasSemana[3], diasSemana[4]);
        System.out.println(linha1 + linha2 + linha2 + linha2 + linha2 + linha2);

        for (int hora = 8; hora < 18; hora++) {
            String limites = linha1;
            String info = "|%2d%3s%3d%s";
            for (int dia = 2; dia < 7; dia++) {
                int ech = ExisteCelulaDiaHora(dia, hora, horario);
                CelulaHorario c = RetornarCelulaDiaHora(dia, hora, horario);
                if (ech == 1) {
                    info += String.format(infoCelula, c.getDisciplina().getSigla(), c.getTipoAula(), c.getSala().getCodigo(), c.getProfessor().getSigla(), c.getTurma().getDesignacao(), separador);
                    limites += linha2;
                } else if (ech == 2) {
                    info += String.format(infoCelula, c.getDisciplina().getSigla(), c.getTipoAula(), c.getSala().getCodigo(), c.getProfessor().getSigla(), c.getTurma().getDesignacao(), separador);
                    limites += linha3;
                } else if (ech == 0) {
                    info += String.format(infoCelula, "   ", " ", "    ", "   ", "   ", separador);
                    limites += linha2;
                }
            }
            if (hora < 17) {
                System.out.format(info, hora, "-", (hora + 1), separador);
                System.out.println("\n" + limites);
            } else if (hora == 17) {
                System.out.format(info, hora, "-", (hora + 1), separador);
                System.out.println("\n+-----------------------------------------------------------------------------------------------------------------------------------------------+");
            }
        }
        System.out.println("\n");
    }

    public static int ExisteCelulaDiaHora(int dia, int hora, ArrayList<CelulaHorario> horario) {
        for (CelulaHorario c : horario) {
            if (c.getDiaSemana() == dia && c.getHoraInicio() == hora) {
                if (c.getDuracao() == 1) {
                    return 1;
                } else if (c.getDuracao() == 2) {
                    return 2;
                }
            }
        }
        return 0; //0 significa que a célula não existe
    }

    public static CelulaHorario RetornarCelulaDiaHora(int dia, int hora, ArrayList<CelulaHorario> horario) {
        CelulaHorario celula = new CelulaHorario();
        for (CelulaHorario c : horario) {
            if (c.getDiaSemana() == dia && c.getHoraInicio() == hora) {
                celula = c;
            }
        }
        return celula;
    }

    public static Aluno RetornarAluno(int numeroAluno) {
        Aluno aluno = new Aluno();
        for (Aluno a : alunos) {
            if (a.getNumero() == numeroAluno) {
                aluno = a.clone();
                break;
            }
        }
        return aluno;
    }

    public static Professor RetornarProfessor(String siglaProfessor) {
        Professor professor = new Professor();
        for (Professor p : professores) {
            if (p.getSigla().equals(siglaProfessor)) {
                professor = p.clone();
                break;
            }
        }
        return professor;
    }

    public static Disciplina RetornarDisciplina(String siglaDisciplina) {
        Disciplina disciplina = new Disciplina();
        for (Disciplina d : disciplinas) {
            if (d.getSigla().equals(siglaDisciplina)) {
                disciplina = d.clone();
                break;
            }
        }
        return disciplina;
    }

    public static Turma RetornarTurma(String designacaoTurma) {
        Turma turma = new Turma();
        for (Turma t : turmas) {
            if (t.getDesignacao().equals(designacaoTurma)) {
                turma = t.clone();
                break;
            }
        }
        return turma;
    }

    public static Sala RetornarSala(String codigoSala) {
        Sala sala = new Sala();
        for (Sala s : salas) {
            if (s.getCodigo().equals(codigoSala)) {
                sala = s.clone();
                break;
            }
        }
        return sala;
    }

    public static CelulaHorario RetornarCelulaHorario(String designacaoTurma, String siglaDisciplina, String tipoAula, int diaSemana, int horaInicio, int duracao, String siglaProfessor, String codigoSala) {
        CelulaHorario celula = new CelulaHorario();
        for (CelulaHorario c : celulashorarios) {
            if (c.getTurma().getDesignacao().equals(designacaoTurma) && c.getDisciplina().getSigla().equals(siglaDisciplina) && c.getTipoAula() == CelulaHorario.TipoAula.valueOf(tipoAula) && c.getDiaSemana() == diaSemana && c.getHoraInicio() == horaInicio && c.getDuracao() == duracao && c.getProfessor().getSigla().equals(siglaProfessor) && c.getSala().getCodigo().equals(codigoSala)) {
                celula = c.clone();
                break;
            }
        }
        return celula;
    }

    public static void InserirCelulaTurma(String designacaoTurma, String siglaDisciplina, String tipoAula, int diaSemana, int horaInicio, int duracao, String siglaProfessor, String codigoSala) throws DadoInvalidoException {
        //valida a existência da turma, disciplina, professor e sala e verifica a validade do dia da semana, hora início e duracao
        ValidarDiaSemana(diaSemana);
        ValidarHoraInicioEDuracao(horaInicio, duracao);
        ValidarTurma(designacaoTurma);
        ValidarDisciplina(siglaDisciplina);
        ValidarProfessor(siglaProfessor);
        ValidarSala(codigoSala);

        //verificar disponibilidade da turma
        ArrayList<CelulaHorario> horarioTurmaDia = HorarioTurmaDia(diaSemana, designacaoTurma);
        int[] horasOcupadasTurma = ConstruirArrayHorasDeUmDia(horarioTurmaDia);
        DisponibilidadeTurma(horasOcupadasTurma, diaSemana, horaInicio, duracao, designacaoTurma);

        //verificar disponibilidade da sala neste dia/hora
        ArrayList<CelulaHorario> horarioSalaDia = HorarioSalaDia(diaSemana, codigoSala);
        int[] horasOcupadasSala = ConstruirArrayHorasDeUmDia(horarioSalaDia);
        DisponibilidadeSala(horasOcupadasSala, diaSemana, horaInicio, duracao, codigoSala);

        //verificar disponibilidade do professor neste dia/hora
        ArrayList<CelulaHorario> horarioProfessorDia = HorarioProfessorDia(diaSemana, siglaProfessor);
        int[] horasOcupadasProfessor = ConstruirArrayHorasDeUmDia(horarioProfessorDia);
        DisponibilidadeProfessor(horasOcupadasProfessor, diaSemana, horaInicio, duracao, siglaProfessor);

        //verificar se o tipo de aula corresponde ao tipo de sala (validando também a existência do tipo de aula)
        Sala s = RetornarSala(codigoSala);
        ValidarTipoAulaComTipoSala(s, tipoAula);

        //criar objectos para o novo objecto CelulaHorario
        Turma t = RetornarTurma(designacaoTurma);
        Disciplina d = RetornarDisciplina(siglaDisciplina);
        Professor p = RetornarProfessor(siglaProfessor);

        celulashorarios.add(new CelulaHorario(t, d, CelulaHorario.TipoAula.valueOf(tipoAula), diaSemana, horaInicio, duracao, p, s));
    }

    public static ArrayList<CelulaHorario> HorarioTurmaDia(int diaSemana, String designacaoTurma) {
        ArrayList<CelulaHorario> horarioTurmaDia = new ArrayList();
        for (CelulaHorario c : celulashorarios) {
            if (c.getTurma().getDesignacao().equals(designacaoTurma) && c.getDiaSemana() == diaSemana) {
                horarioTurmaDia.add(c);
            }
        }
        return horarioTurmaDia;
    }

    public static ArrayList<CelulaHorario> HorarioSalaDia(int diaSemana, String codigoSala) { //horário de uma sala apenas de um determinado dia
        ArrayList<CelulaHorario> horarioSalaDia = new ArrayList();

        for (CelulaHorario c : celulashorarios) {
            if (c.getDiaSemana() == diaSemana && c.getSala().getCodigo().equals(codigoSala)) {
                horarioSalaDia.add(c);
            }
        }
        return horarioSalaDia;
    }

    public static ArrayList<CelulaHorario> HorarioProfessorDia(int diaSemana, String siglaProfessor) {
        ArrayList<CelulaHorario> horarioProfessorDia = new ArrayList();
        for (CelulaHorario c : celulashorarios) {
            if (c.getProfessor().getSigla().equals(siglaProfessor) && c.getDiaSemana() == diaSemana) {
                horarioProfessorDia.add(c);
            }
        }
        return horarioProfessorDia;
    }

    public static int[] ConstruirArrayHorasDeUmDia(ArrayList<CelulaHorario> horario) {
        int[] horasOcupadas = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //cada indice representa 1 hora do horario
        for (CelulaHorario c : horario) {
            int i = c.getHoraInicio() - 8;
            if (c.getDuracao() == 2) {
                horasOcupadas[i] = 1;
                horasOcupadas[i + 1] = 1;
            } else if (c.getDuracao() == 1) {
                horasOcupadas[i] = 1;
            }
        }
        return horasOcupadas;
    }

    public static void DisponibilidadeTurma(int[] horasOcupadasTurma, int diaSemana, int horaInicio, int duracao, String designacaoTurma) throws DadoInvalidoException {
        if (duracao == 1 && horasOcupadasTurma[horaInicio - 8] == 1) {
            throw new DadoInvalidoException("Turma " + designacaoTurma + " indisponível " + Data.diaSemana2(diaSemana - 1) + "/" + horaInicio + ":00");
        } else if ((duracao == 2) && (horasOcupadasTurma[horaInicio - 8] == 1 || horasOcupadasTurma[horaInicio + 1 - 8] == 1)) {
            if (horasOcupadasTurma[horaInicio - 8] == 1) {
                throw new DadoInvalidoException("Turma " + designacaoTurma + " indisponível " + Data.diaSemana2(diaSemana - 1) + "/" + horaInicio + ":00");
            } else if (horasOcupadasTurma[horaInicio + 1 - 8] == 1) {
                throw new DadoInvalidoException("Turma " + designacaoTurma + " indisponível " + Data.diaSemana2(diaSemana - 1) + "/" + (horaInicio + 1) + ":00");
            }
        }
    }

    public static void DisponibilidadeSala(int[] horasOcupadasSala, int diaSemana, int horaInicio, int duracao, String codigoSala) throws DadoInvalidoException {
        if (duracao == 1 && horasOcupadasSala[horaInicio - 8] == 1) {
            throw new DadoInvalidoException("Sala " + codigoSala + " ocupada " + Data.diaSemana2(diaSemana - 1) + "/" + horaInicio + ":00");
        } else if ((duracao == 2) && (horasOcupadasSala[horaInicio - 8] == 1 || horasOcupadasSala[horaInicio + 1 - 8] == 1)) {
            if (horasOcupadasSala[horaInicio - 8] == 1) {
                throw new DadoInvalidoException("Sala " + codigoSala + " ocupada " + Data.diaSemana2(diaSemana - 1) + "/" + horaInicio + ":00");
            } else if (horasOcupadasSala[horaInicio + 1 - 8] == 1) {
                throw new DadoInvalidoException("Sala " + codigoSala + " ocupada " + Data.diaSemana2(diaSemana - 1) + "/" + (horaInicio + 1) + ":00");
            }
        }
    }

    public static void DisponibilidadeProfessor(int[] horasOcupadasProfessor, int diaSemana, int horaInicio, int duracao, String siglaProfessor) throws DadoInvalidoException {
        if (duracao == 1 && horasOcupadasProfessor[horaInicio - 8] == 1) {
            throw new DadoInvalidoException("Professor(a) " + siglaProfessor + " indisponível " + Data.diaSemana2(diaSemana - 1) + "/" + horaInicio + ":00");
        } else if ((duracao == 2) && (horasOcupadasProfessor[horaInicio - 8] == 1 || horasOcupadasProfessor[horaInicio + 1 - 8] == 1)) {
            if (horasOcupadasProfessor[horaInicio - 8] == 1) {
                throw new DadoInvalidoException("Professor(a) " + siglaProfessor + " indisponível " + Data.diaSemana2(diaSemana - 1) + "/" + horaInicio + ":00");
            } else if (horasOcupadasProfessor[horaInicio + 1 - 8] == 1) {
                throw new DadoInvalidoException("Professor(a) " + siglaProfessor + " indisponível " + Data.diaSemana2(diaSemana - 1) + "/" + (horaInicio + 1) + ":00");
            }
        }
    }

    public static void AlterarCelulaTurma(String designacaoTurma, String siglaDisciplina, String tipoAula, int diaSemana, int horaInicio, int duracao, String siglaProfessor, String codigoSala, String designacaoTurmaNew, String siglaDisciplinaNew, String tipoAulaNew, int diaSemanaNew, int horaInicioNew, int duracaoNew, String siglaProfessorNew, String codigoSalaNew) throws DadoInvalidoException {
        //valida a existencia da célula a alterar
        ValidarCelulaHorario(designacaoTurma, siglaDisciplina, tipoAula, diaSemana, horaInicio, duracao, siglaProfessor, codigoSala);

        //valida a existência da turma, disciplina, professor e sala e verifica a validade do dia da semana, hora início e duracao (os novos atributos para modificações à célula)
        ValidarDiaSemana(diaSemanaNew);
        ValidarHoraInicioEDuracao(horaInicio, duracaoNew);
        ValidarTurma(designacaoTurmaNew);
        ValidarDisciplina(siglaDisciplinaNew);
        ValidarProfessor(siglaProfessorNew);
        ValidarSala(codigoSalaNew);

        //retorna a célula a alterar
        CelulaHorario celula = RetornarCelulaHorario(designacaoTurma, siglaDisciplina, tipoAula, diaSemana, horaInicio, duracao, siglaProfessor, codigoSala);

        //verificar se o tipo de aula corresponde ao tipo de sala (validando também a existência do tipo de aula)
        Sala s = RetornarSala(codigoSalaNew);
        ValidarTipoAulaComTipoSala(s, tipoAulaNew);

        try {
            EliminarCelulaTurma(celula.getTurma().getDesignacao(), celula.getDiaSemana(), celula.getHoraInicio());
            InserirCelulaTurma(designacaoTurmaNew, siglaDisciplinaNew, tipoAulaNew, diaSemanaNew, horaInicioNew, duracaoNew, siglaProfessorNew, codigoSalaNew);

        } catch (DadoInvalidoException ex) {
            InserirCelulaTurma(designacaoTurma, siglaDisciplina, tipoAula, diaSemana, horaInicio, duracao, siglaProfessor, codigoSala);
            throw ex;
        }
    }

    public static void OrdenarAlunosTurmaCrescenteNumero(String designacaoTurma) throws DadoInvalidoException {
        ValidarTurma(designacaoTurma);
        Turma turma = RetornarTurma(designacaoTurma);
        Comparator crescente = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                int numero1 = ((Aluno) o1).getNumero();
                int numero2 = ((Aluno) o2).getNumero();
                return (int) numero1 - numero2;
            }
        };
        Collections.sort(turma.getAlunos(), crescente);
    }

    public static void OrdenarAlunosTurmaDecrescenteNumero(String designacaoTurma) throws DadoInvalidoException {
        ValidarTurma(designacaoTurma);
        Turma turma = RetornarTurma(designacaoTurma);
        Comparator decrescente = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                int numero1 = ((Aluno) o1).getNumero();
                int numero2 = ((Aluno) o2).getNumero();
                return (int) numero2 - numero1;
            }
        };
        Collections.sort(turma.getAlunos(), decrescente);
    }

    public static void OrdenarAlunosTurmaAscendenteNome(String designacaoTurma) throws DadoInvalidoException {
        ValidarTurma(designacaoTurma);
        Turma turma = RetornarTurma(designacaoTurma);
        Comparator ascendente = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Aluno a1 = ((Aluno) o1);
                Aluno a2 = ((Aluno) o2);
                return (int) (a1.getNome().compareTo(a2.getNome()));
            }
        };
        Collections.sort(turma.getAlunos(), ascendente);
    }

    public static void OrdenarAlunosTurmaDescendenteNome(String designacaoTurma) throws DadoInvalidoException {
        ValidarTurma(designacaoTurma);
        Turma turma = RetornarTurma(designacaoTurma);
        Comparator descendente = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Aluno a1 = ((Aluno) o1);
                Aluno a2 = ((Aluno) o2);
                return (int) (a2.getNome().compareTo(a1.getNome()));
            }
        };
        Collections.sort(turma.getAlunos(), descendente);
    }

    public static void ListarAlunosTurma(String designacaoTurma) throws DadoInvalidoException {
        ValidarTurma(designacaoTurma);
        Turma turma = RetornarTurma(designacaoTurma);
        for (Aluno a : turma.getAlunos()) {
            System.out.println(a);
        }
    }

    public static String RetornarTurmaDoAluno(int numeroAluno) {
        String designacaoTurma = "";
        ArrayList<Aluno> alunosTurma;
        for (Turma t : turmas) {
            alunosTurma = t.getAlunos();
            for (Aluno a : alunosTurma) {
                if (a.getNumero() == numeroAluno) {
                    designacaoTurma = t.getDesignacao();
                    break;
                }
            }
        }
        return designacaoTurma;
    }

    public static ArrayList<CelulaHorario> HorarioTurma(String designacaoTurma) {
        ArrayList<CelulaHorario> horario = new ArrayList();
        for (CelulaHorario c : celulashorarios) {
            if (c.getTurma().getDesignacao().equals(designacaoTurma)) {
                horario.add(c);
            }
        }
        return horario;
    }

    public static ArrayList<CelulaHorario> HorarioProfessor(String siglaProfessor) {
        ArrayList<CelulaHorario> horario = new ArrayList();
        for (CelulaHorario c : celulashorarios) {
            if (c.getProfessor().getSigla().equals(siglaProfessor)) {
                horario.add(c);
            }
        }
        return horario;
    }

    public static ArrayList<CelulaHorario> HorarioDisciplina(String siglaDisciplina) {
        ArrayList<CelulaHorario> horario = new ArrayList();
        for (CelulaHorario c : celulashorarios) {
            if (c.getDisciplina().getSigla().equals(siglaDisciplina)) {
                horario.add(c);
            }
        }
        return horario;
    }

    public static ArrayList<CelulaHorario> HorarioSala(String codigoSala) {
        ArrayList<CelulaHorario> horario = new ArrayList();
        for (CelulaHorario c : celulashorarios) {
            if (c.getSala().getCodigo().equals(codigoSala)) {
                horario.add(c);
            }
        }
        return horario;
    }

    public static void ImprimirHorarioAluno(int numeroAluno) throws DadoInvalidoException {
        ValidarAluno(numeroAluno);
        ImprimirHorario(HorarioTurma(RetornarTurmaDoAluno(numeroAluno)));
    }

    public static void ImprimirHorarioProfessor(String siglaProfessor) throws DadoInvalidoException {
        ValidarProfessor(siglaProfessor);
        ImprimirHorario(HorarioProfessor(siglaProfessor));
    }

    public static void ImprimirHorarioTurma(String designcaoTurma) throws DadoInvalidoException {
        ValidarTurma(designcaoTurma);
        ImprimirHorario(HorarioTurma(designcaoTurma));
    }

    public static void ImprimirHorarioDisciplina(String siglaDisciplina) throws DadoInvalidoException {
        ValidarDisciplina(siglaDisciplina);
        ImprimirHorario(HorarioDisciplina(siglaDisciplina));
    }

    public static void ImprimirHorarioSala(String codigoSala) throws DadoInvalidoException {
        ValidarSala(codigoSala);
        ImprimirHorario(HorarioSala(codigoSala));
    }

    public static int CargaHorariaSemanalAluno(int numeroAluno) throws DadoInvalidoException {
        ValidarAluno(numeroAluno);
        String turmaAluno = RetornarTurmaDoAluno(numeroAluno);
        int cargaHoraria = 0;
        for (CelulaHorario c : celulashorarios) {
            if (c.getTurma().getDesignacao().equals(turmaAluno)) {
                cargaHoraria = cargaHoraria + c.getDuracao();
            }
        }
        return cargaHoraria;
    }

    public static int CargaHorariaSemanalProfessor(String siglaProfessor) throws DadoInvalidoException {
        ValidarProfessor(siglaProfessor);
        int cargaHoraria = 0;
        for (CelulaHorario c : celulashorarios) {
            if (c.getProfessor().getSigla().equals(siglaProfessor)) {
                cargaHoraria = cargaHoraria + c.getDuracao();
            }
        }
        return cargaHoraria;
    }

    public static int CargaHorariaSemanalTurma(String designacaoTurma) throws DadoInvalidoException {
        ValidarTurma(designacaoTurma);
        int cargaHoraria = 0;
        for (CelulaHorario c : celulashorarios) {
            if (c.getTurma().getDesignacao().equals(designacaoTurma)) {
                cargaHoraria = cargaHoraria + c.getDuracao();
            }
        }
        return cargaHoraria;
    }

    public static int CargaHorariaSemanalDisciplina(String siglaDisciplina) throws DadoInvalidoException {
        ValidarDisciplina(siglaDisciplina);
        int cargaHoraria = 0;
        for (CelulaHorario c : celulashorarios) {
            if (c.getDisciplina().getSigla().equals(siglaDisciplina)) {
                cargaHoraria = cargaHoraria + c.getDuracao();
            }
        }
        return cargaHoraria;
    }

    public static int CargaHorariaSemanalSala(String codigoSala) throws DadoInvalidoException {
        ValidarSala(codigoSala);
        int cargaHoraria = 0;
        for (CelulaHorario c : celulashorarios) {
            if (c.getSala().getCodigo().equals(codigoSala)) {
                cargaHoraria = cargaHoraria + c.getDuracao();
            }
        }
        return cargaHoraria;
    }

    public static int[] EncontrarPeriodoDisponivel(String codigoSala, int diaSemana, int horaInicio) throws DadoInvalidoException {
        ValidarSala(codigoSala);
        ValidarDiaSemana(diaSemana);
        ValidarHoraInicio(horaInicio);

        int horaInicial = 0; //desde que hora a sala está disponível
        int numeroHorasDisponiveis = 0;
        ArrayList<CelulaHorario> horarioSalaDia = HorarioSalaDia(diaSemana, codigoSala);
        int[] horasOcupadasSala = ConstruirArrayHorasDeUmDia(horarioSalaDia);

        int j = 0;
        //encontra posição primeiro 0 a partir da hora inicio indicada pelo utilizador
        for (int i = horaInicio - 8; i < horasOcupadasSala.length; i++) {
            if (horasOcupadasSala[i] == 0) {
                j = i;
                break;
            }
        }

        //atribuí a hora inicial
        horaInicial = j + 8;

        //conta o número de horas disponiveis
        while (j < horasOcupadasSala.length && horasOcupadasSala[j] == 0) {
            numeroHorasDisponiveis++;
            j++;
        }

        int[] str = {horaInicial, numeroHorasDisponiveis};
        if (numeroHorasDisponiveis == 0) {
            throw new DadoInvalidoException("Sala " + codigoSala + " sem disponibilidade a partir das " + horaInicio + ":00");
        }
        return str;
    }

    public static ArrayList<Sala> OcupacaoSemanalSala(int percentagemOcupacao) throws DadoInvalidoException {
        if (percentagemOcupacao < 0 || percentagemOcupacao > 100) {
            throw new DadoInvalidoException("Percentagem inválida");
        }
        double po = percentagemOcupacao / 100.0;
        ArrayList<Sala> salasOcupadas = new ArrayList();
        for (Sala s : salas) {
            if (CargaHorariaSemanalSala(s.getCodigo()) > 50 * po) {
                salasOcupadas.add(s);
            }
        }
        return salasOcupadas;
    }

    public static void ListarSalasOcupadas(int percentagemOcupacao) throws DadoInvalidoException {
        ArrayList<Sala> salasOcupadas = OcupacaoSemanalSala(percentagemOcupacao);
        if (!salasOcupadas.isEmpty()) {
            for (Sala s : salasOcupadas) {
                System.out.println(s.getCodigo() + " - " + ((float) CargaHorariaSemanalSala(s.getCodigo()) / 50 * 100) + "%");
            }
        } else {
            System.out.println("Não há salas com ocupação superior a " + percentagemOcupacao + "%");
        }
    }

    public static ArrayList<String> DisciplinasETurmaLecionaProfessor(String siglaProf) throws DadoInvalidoException {
        ValidarProfessor(siglaProf);
        ArrayList<String> dtlp_aux = new ArrayList();
        for (CelulaHorario c : celulashorarios) {
            if (c.getProfessor().getSigla().equals(siglaProf)) {
                dtlp_aux.add(String.format("%s - %s", c.getDisciplina().getSigla(), c.getTurma().getDesignacao()));
            }
        }

        //remover repetidos da arraylist ppt_aux
        ArrayList<String> dlp = new ArrayList();
        for (String s : dtlp_aux) {
            if (!dlp.contains(s)) {
                dlp.add(s);
            }
        }

        //ordena a arraylist
        Collections.sort(dlp);
        return dlp;
    }

    public static void ListarDisciplinasLecionadasProfessor(String siglaProf) throws DadoInvalidoException {
        ValidarProfessor(siglaProf);
        ArrayList<String> dtlp = DisciplinasETurmaLecionaProfessor(siglaProf);
        if (!dtlp.isEmpty()) {
            System.out.println("O professor " + siglaProf + " leciona as seguintes disciplinas às turmas: ");
            for (String s : dtlp) {
                System.out.println(s);
            }
        } else {
            System.out.println("O professor " + siglaProf + " não leciona nenhum disciplina");
        }
    }

    public static ArrayList<Disciplina> DisciplinasSala(String codigoSala) throws DadoInvalidoException {
        ValidarSala(codigoSala);
        ArrayList<Disciplina> ds_aux = new ArrayList();
        for (CelulaHorario c : celulashorarios) {
            if (c.getSala().getCodigo().equals(codigoSala)) {
                ds_aux.add(c.getDisciplina());
            }
        }

        //remover repetidos da arraylist ds_aux
        ArrayList<Disciplina> ds = new ArrayList();
        for (Disciplina d : ds_aux) {
            if (!ds.contains(d)) {
                ds.add(d);
            }
        }
        return ds;
    }

    public static void ListarDisciplinasSala(String codigoSala) throws DadoInvalidoException {
        ValidarSala(codigoSala);
        ArrayList<Disciplina> ds = DisciplinasSala(codigoSala);
        if (!ds.isEmpty()) {
            System.out.println("A sala " + codigoSala + " tem as seguintes disciplinas a decorrerem:");
            for (Disciplina d : ds) {
                System.out.println(d.getSigla() + " - " + d.getDesignacao());
            }
        } else {
            System.out.println("A sala " + codigoSala + " não tem disciplinas associadas");
        }
    }

    public static ArrayList<String> ProfessoresTurma(String designacaoTurma) throws DadoInvalidoException {
        ValidarTurma(designacaoTurma);
        ArrayList<String> pt_aux = new ArrayList();
        for (CelulaHorario c : celulashorarios) {
            if (c.getTurma().getDesignacao().equals(designacaoTurma)) {
                pt_aux.add(String.format("%s - %s - %s", c.getProfessor().getSigla(), c.getDisciplina().getSigla(), c.getTipoAula()));
            }
        }

        //remover repetidos da arraylist pt_aux
        ArrayList<String> pt = new ArrayList();
        for (String s : pt_aux) {
            if (!pt.contains(s)) {
                pt.add(s);
            }
        }

        //ordena a arraylist
        Collections.sort(pt);

        return pt;
    }

    public static void ListarProfessoresTurma(String designacaoTurma) throws DadoInvalidoException { //Listar os professores que dão aulas a uma determinada turma, indicando as respetivas disciplinas e tipos de aula.
        ValidarTurma(designacaoTurma);
        ArrayList<String> pt = ProfessoresTurma(designacaoTurma);
        if (!pt.isEmpty()) {
            System.out.println("A turma  " + designacaoTurma + " tem os seguintes professores a lecionarem as diciplinas:");
            for (String s : pt) {
                System.out.println(s);
            }
        } else {
            System.out.println("A turma " + designacaoTurma + " não tem aulas associadas"); //logo não tem professores
        }
    }

    public static void ListarDadosAluno(int numeroAluno) throws DadoInvalidoException {
        ValidarAluno(numeroAluno);
        System.out.println(RetornarAluno(numeroAluno).toString());
    }

    public static void ListarDadosProfessor(String siglaProfessor) throws DadoInvalidoException {
        ValidarProfessor(siglaProfessor);
        System.out.println(RetornarProfessor(siglaProfessor));
    }

    public static void ListarDadosTurma(String designacaoTurma) throws DadoInvalidoException {
        ValidarTurma(designacaoTurma);
        System.out.println(RetornarTurma(designacaoTurma));
    }

    public static void ListarDadosDisciplina(String siglaDisciplina) throws DadoInvalidoException {
        ValidarDisciplina(siglaDisciplina);
        System.out.println(RetornarDisciplina(siglaDisciplina));
    }

    public static void ListarDadosSala(String codigoSala) throws DadoInvalidoException {
        ValidarSala(codigoSala);
        System.out.println(RetornarSala(codigoSala));
    }

    public static ArrayList<String> DesignacoesTodasTurmas() {
        ArrayList<String> designacoes = new ArrayList();
        for (Turma t : turmas) {
            designacoes.add(t.getDesignacao());
        }
        Collections.sort(designacoes);
        return designacoes;
    }

    public static ArrayList<String> SiglasTodasDisciplinas() {
        ArrayList<String> siglas = new ArrayList();
        for (Disciplina d : disciplinas) {
            siglas.add(d.getSigla());
        }
        Collections.sort(siglas);
        return siglas;
    }

    public static ArrayList<String> SiglasTodosProfessores() {
        ArrayList<String> siglas = new ArrayList();
        for (Professor p : professores) {
            siglas.add(p.getSigla());
        }
        Collections.sort(siglas);
        return siglas;
    }

    public static ArrayList<String> CodigosTodasSalas() {
        ArrayList<String> codigos = new ArrayList();
        for (Sala s : salas) {
            codigos.add(s.getCodigo());
        }
        Collections.sort(codigos);
        return codigos;
    }

    public static ArrayList<String> NumerosTodosAlunos() {
        ArrayList<String> numeros = new ArrayList();
        for (Aluno a : alunos) {
            numeros.add("" + a.getNumero());
        }
        Collections.sort(numeros);
        return numeros;
    }

    public static CelulaHorario RetornarCelulaPelaTurmaDiaHora(String designacaoTurma, int diaSemana, int horaInicio) throws DadoInvalidoException {
        CelulaHorario celula = new CelulaHorario();
        int cont = 1;
        for (CelulaHorario c : celulashorarios) {
            if (c.getTurma().getDesignacao().equals(designacaoTurma) && c.getDiaSemana() == diaSemana && c.getHoraInicio() == horaInicio) {
                celula = c.clone();
                break;
            } else if (cont == celulashorarios.size()) {
                throw new DadoInvalidoException("Célula da turma " + designacaoTurma + " " + Data.diaSemana2(diaSemana - 1) + "/" + horaInicio + ":00 inexistente");
            }
            cont++;
        }
        return celula;
    }

    public static ArrayList<String> SiglasProfesoresDeUmaDisciplina(String siglaDisciplina) {
        Disciplina d = RetornarDisciplina(siglaDisciplina);
        ArrayList<String> siglasProfessores = new ArrayList();
        for (Professor p : d.getProfessores()) {
            siglasProfessores.add(p.getSigla());
        }
        Collections.sort(siglasProfessores);
        return siglasProfessores;
    }

    public static ArrayList<String> NumerosAlunosDeUmTurma(String designacaoTurma) {
        Turma t = RetornarTurma(designacaoTurma);
        ArrayList<String> numerosAlunos = new ArrayList();
        for (Aluno a : t.getAlunos()) {
            numerosAlunos.add("" + a.getNumero());
        }
        Collections.sort(numerosAlunos);
        return numerosAlunos;
    }

    public static String[][] PreencherMatrizHorario(ArrayList<CelulaHorario> horario) throws DadoInvalidoException {
        String mat[][] = new String[11][5];

        for (int hora = 8; hora < 18; hora++) {
            for (int dia = 2; dia < 7; dia++) {
                int ech = Horario.ExisteCelulaDiaHora(dia, hora, horario);
                CelulaHorario c = Horario.RetornarCelulaDiaHora(dia, hora, horario);
                if (ech == 1) {
                    mat[hora - 8][dia - 2] = c.getDisciplina().getSigla() + " - " + c.getTipoAula() + " - " + c.getSala().getCodigo() + " - " + c.getProfessor().getSigla() + " - " + c.getTurma().getDesignacao();
                } else if (ech == 2) {
                    mat[hora - 8][dia - 2] = c.getDisciplina().getSigla() + " - " + c.getTipoAula() + " - " + c.getSala().getCodigo() + " - " + c.getProfessor().getSigla() + " - " + c.getTurma().getDesignacao();

                    if (c.getTipoAula() == CelulaHorario.TipoAula.P) {
                        mat[hora + 1 - 8][dia - 2] = "  ";//2 espaços = prática de 2 horas
                    } else if (c.getTipoAula() == CelulaHorario.TipoAula.T) {
                        mat[hora + 1 - 8][dia - 2] = "   ";//3 espaços = teóricas de 2 horas
                    }
                }
            }
        }
        return mat;
    }

    public static void CarregarFicheirosOriginais() throws FileNotFoundException {
        //Apaga conteudo actual dos contentores
        alunos.clear();
        professores.clear();
        salas.clear();
        disciplinas.clear();
        turmas.clear();
        celulashorarios.clear();

        //repõe nos contentores os dados originais
        LerFicheiroAlunos();
        LerFicheiroProfessores();
        LerFicheiroSalas();
        LerFicheiroTurmas();
        LerFicheiroDisciplinas();
        LerFicheiroCelulasHorarios();

    }

    public static void gravarEstadoActual() throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("estado.bin"));
        out.writeObject(alunos);
        out.writeObject(professores);
        out.writeObject(salas);
        out.writeObject(turmas);
        out.writeObject(disciplinas);
        out.writeObject(celulashorarios);
        out.close();

    }

    public static void lerEstadoAnterior() throws IOException, ClassNotFoundException, FileNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("estado.bin"));
        alunos = (ArrayList<Aluno>) in.readObject();
        professores = (ArrayList<Professor>) in.readObject();
        salas = (ArrayList<Sala>) in.readObject();
        turmas = (ArrayList<Turma>) in.readObject();
        disciplinas = (ArrayList<Disciplina>) in.readObject();
        celulashorarios = (ArrayList<CelulaHorario>) in.readObject();
        in.close();
    }

    public static void FicheirosALerNoInicio() throws IOException, ClassNotFoundException {
        try {
            lerEstadoAnterior();
        } catch (FileNotFoundException ex) {
            //lê os ficheiros txt se não encontrar o estado.bin
            LerFicheiroAlunos();
            LerFicheiroProfessores();
            LerFicheiroSalas();
            LerFicheiroTurmas();
            LerFicheiroDisciplinas();
            LerFicheiroCelulasHorarios();
//            throw new FileNotFoundException("Ficheiro 'estado.bin' não encontrado na pasta da aplicação");
        }
    }
}