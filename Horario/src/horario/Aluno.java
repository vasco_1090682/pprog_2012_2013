package horario;

import utilitarios.Data;

public class Aluno extends Pessoa {
    //Variáveis de classe

    //Variáveis de instância
    private int numero;
    private Data datanascimento;
    private int telefone;

    //Construtores
    public Aluno(String nome, String email, int numero, Data datanascimento, int telefone) {
        super(nome, email);
        setNumero(numero);
        setDatanascimento(datanascimento);
        setTelefone(telefone);
    }

    public Aluno() {
        this("", "", 0, new Data(), 0);
    }

    public Aluno(Aluno a) {
        this(a.getNome(), a.getEmail(), a.numero, a.datanascimento, a.telefone);
    }

    //Métodos de classe
    //Métodos de consulta

    //Métodos modificadores

    //Métodos de instância
    //Métodos de consulta
    public int getNumero() {
        return numero;
    }

    public Data getDatanascimento() {
        return datanascimento;
    }

    public int getTelefone() {
        return telefone;
    }

    //Métodos modificadores
    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setDatanascimento(Data datanascimento) {
        this.datanascimento = datanascimento;
    }

    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }
    
    //Métodos complementares
    public String toString(){
        return super.toString() + " | Número: " + numero + " | Data nascimento: " + datanascimento.toAnoMesDiaString() + " | Telefone: " + telefone;
    }
    
    public Aluno clone() {
        return new Aluno(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Aluno a = (Aluno) obj;
        return getNome().equals(a.getNome()) && getEmail().equals(a.getEmail()) && numero == a.numero && datanascimento == a.datanascimento && telefone == a.telefone;
    }
}
